import React, { Component,AsyncStorage} from 'react';
const Toast = props => {
    const {
        toast,
        Stext,
        Sbody,
        ...attributes
        } = props;

    return (
        <div style={{opacity:toast?"1":"0"}} className="outeralert">
            <div className={"bouncetop "+(toast?"show":"")}>
          <div className="alert alert-dark">
            <strong>{Stext}!</strong> {Sbody}
        </div>
        </div>
        </div>
    )
}
export default Toast;