import * as firebase from "firebase/app";
import "firebase/messaging";

const initializedFirebaseApp = firebase.initializeApp({
	// Project Settings => Add Firebase to your web app
  messagingSenderId: "163150971763"
});
const messaging = initializedFirebaseApp.messaging();
messaging.usePublicVapidKey(
	// Project Settings => Cloud Messaging => Web Push certificates
  "BGC4mnKtX73QDJFk5hDo0yCkXyh_wKyMcHV2zwLS-XGk1n2AEc_2DygHTNdlDODulWcIyLWO_avjWtkJzRm7iL8"
);
console.log('messaging :-'+messaging)
export { messaging };