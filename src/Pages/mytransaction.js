import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
import {Link} from 'react-router-dom';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import StarRatings from 'react-star-ratings';
import {retrieveItem} from '../component/retriveitem';
import Toast from '../utils/toast'
import Loader from '../utils/loader'

class Mytrasaction extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
        muted: true,
      loadsearData:[],
      modalVisible:false,
      modalVisible1:false,
      setpricevisible:false,
      modalVisiblecc:false,
      loadsk:false
      }
      this.getusertype();
  }
  
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
            currentdata:item,
            // otp: item.otp_verified,
            name: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        this.loadmaster(this.state.id,this.state.token)
        this.notification(this.state.id,this.state.token);
    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
    
    // alert('Welcome, '+this.state.name)
    // alert(this.state.getid)
    
}
loadmaster=async(id,token)=>{
    var url=constant.SER_URL+'/celebrity-transaction-list';
    await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization: 'Bearer' + token,
        },
        body: JSON.stringify({
            celebrity_id: id,
        })
    })
        .then((response) => response.json())
        .then((response) =>{
          var resmsg = response['code'];
          if (resmsg == "200"){
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            this.setState({
              mytranslist:response['data']['transactionList']
            })
            setTimeout(() => {
                this.setState({
                    loadsk:true
                })
            }, 1000);
            console.log(JSON.stringify(response['data']['transactionList']), 1000);
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          
          }
          else if (resmsg == "422") {
            this.setState({loading: true}); 
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            
            console.log(JSON.stringify(response['data']));
          }
        }).catch(error => {
        //   console.log('error: '+error, 1000);
        alert('Server Error')
        this.setState({
            loading: false
        });
    });
  }

  loop = () => {
      let customCard = [];
      for (let i = 0; i < 10; i++){
          customCard.push(
              <div key={i} className="col-lg-2 col-md-3 p-2">
                  <div className="customCard shadow p-3">
                      <div className="w-100 pull-left">
                          <div className="bookedCeleb2 radius-full pull-right">
                              <img src={require('../img/celebrity1.png')} alt='test' />
                          </div>
                      </div>

                      <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                      <div className="rupees font14 font-weight-bold pull-right prif-color">+₹500</div>
                      <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                  </div>
              </div>
          )
      }
      return customCard;
  }
  notification = async (id,token) => {
  // this.setState({loading: true});
  var url = constant.SER_URL + '/notification-list';
  await fetch(url, {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          authorization:'Bearer'+token,
      },
      body: {
          celebrity_id: id
      }
  })
  .then((response) => response.json())
  .then((response) => {
    console.log("notification", response)
          this.setState({
              notifydata: response['data']
          })
      })
      .catch(error => console.log(error)) //to catch the errors if any
}
daterender=(param)=>{
  var date = param;
  var datearray1 = date.split(" ");
  var datearray = datearray1[0].split("-");
  var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
  return this.time_ago(newdate)
}
time_ago=(time) =>{

  switch (typeof time) {
    case 'number':
      break;
    case 'string':
      time = +new Date(time);
      break;
    case 'object':
      if (time.constructor === Date) time = time.getTime();
      break;
    default:
      time = +new Date();
  }
  var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hours', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
  ];
  var seconds = (+new Date() - time) / 1000,
    token = 'ago',
    list_choice = 1;

  if (seconds == 0) {
    return 'Just now'
  }
  if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
  }
  var i = 0,
    format;
  while (format = time_formats[i++])
    if (seconds < format[0]) {
      if (typeof format[2] == 'string')
        return format[list_choice];
      else
        return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
  return time;
}
statuspara=(param)=>{
  switch (param) {
    case 'like':
      return 'fa-thumbs-up'
      break;
    case 'comment':
        return 'fa-comment'
        break;
    case 'shoutout-new-request':
      return 'fa-bell'
      break;
    case 'shoutout-request-accepted':
      return 'fa-check'
      break;
    case 'shoutout-request-rejected':
      return 'fa-ban'
      break;
    default:
        return 'fa-thumbs-up'
      break;
  }
}

  render() {
    return (
        <div>
            {/* <Header/> */}
            <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class={this.state.usertype != null || this.state.usertype != undefined?"nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold":"nav-link  pl-0 pr-0 text-white font-weight-bold"} href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  {
                    this.state.usertype != null || this.state.usertype != undefined?
                    <div className="dropdown-menu dropdown-menu-right">
                    <span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                      <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                      {
                        this.state.usertype == '2'?
                        <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                      }
                      
                      <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    </div>:null
                  }
                    
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.notifydata ? this.state.notifydata.length >=3?this.state.notifydata.length+'+':this.state.notifydata.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {
                                                this.state.notifydata  ?
                                                    this.state.notifydata.slice(0,5).map((item, k) =>
                                                        <div class="dropdown-item d-flex align-items-center mainnoti" key={k}>
                                                        <div class="mr-3">
                                                          <div class="icon-circle bg-primary">
                                                            <i class={"fa "+this.statuspara(item.page)+" text-white"} aria-hidden="true"></i>

                                                          </div>
                                                        </div>
                                                        <div>
                                                          <div class="small text-gray-500">{this.daterender(item.created_at)}</div>
                                                          <span>{item.message}</span>
                                                        </div>
                                                      </div>
                                                    ) : <p><center>no notification</center></p>
                                            }
                                            {
                                              this.state.notifydata ?
                                                this.state.notifydata.length > 5 ?
                                                  <center>
                                                    {/* <button className="dropdown-item text-center small text-gray-500"> */}
                                                      <Link
                                                        to="/mynotification" className="dropdown-item text-center small text-gray-500">SEE MORE</Link>
                                                        {/* </button> */}
                                                  </center>
                                                   : null : null
                                             }
                                        </div>
                                    </div>

                                </div>


            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
            <div className="container-fluid pribg-color blueContainer2"></div>
            <div className="container pb-5 border-bottom pt-5">
                <div className="row">
                    <div className="container">
                        <div className="row position-relative">
                            <div className="totalEarning text-center mb-2 position-absolute">
                                <h2 className="text-white font-weight-bold mb-0 text-right">₹<span id='totalpaymentdone'>5000</span></h2>
                                <div className="text-white font10">TOTAL EARNING</div>
                            </div>

                             {/* Nav tabs */}

                            <ul class="nav nav-tabs border-0" role="tablist">
                                <li class="nav-item">
                                <a class="nav-link whiteColor active" data-toggle="tab" href="#paymentdone">PAYMENT DONE</a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link whiteColor" data-toggle="tab" href="#paymentpending">PAYMENT PENDING</a>
                                </li>
                            </ul>

                           {/* Tab panes */}

                            <div class="tab-content w-100 mt-3" id='paymentpage'>
                                <div id="paymentdone" class="container tab-pane active">
                                    <div className="row">
                                    {/* {this.loop()} */}
                                        {
                                            this.state.loadsk ?
                                                this.state.mytranslist.length > 0 ?
                                                    this.state.mytranslist.slice(0).reverse().map((item, k) =>
                                                    item.status == '1'|| item.status == '2' || item.status == '3' || item.status == '4'?
                                                        <div key={k} className="col-lg-2 col-md-3 p-2">
                                                            <div className="customCard shadow p-3">
                                                                <div className="w-100 pull-left">
                                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                                        <img src={item.app_user.thumbnail_profile_pic?item.app_user.thumbnail_profile_pic:require('../img/celebrity1.png')} alt='test' />
                                                                    </div>
                                                                </div>

                                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>{item.app_user.name}</b>

                                                                <div className="rupees font14 font-weight-bold pull-right prif-color">+₹ <span id="add">{item.price}</span></div>
                                                                <div className="w-100 font8 grey-color pull-right text-right">{item.payment_date?this.daterender(item.payment_date):'yyyy-mm-dd'}</div>
                                                            </div>
                                                        </div>:null)
                                                    : <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4 text-center">
                                                        <p>
                                                            {/* user not found */}
                                                            No Transaction </p>
                                                    </div>
                                                : <Newsktn count={12} size={"15%"} sizeH={196} />

                                        }
                                    </div>
                                </div>
                                <div id="paymentpending" class="container tab-pane fade">
                                    <div className="row">
                                    {
                                            this.state.loadsk ?
                                                this.state.mytranslist.length > 0 ?
                                                    this.state.mytranslist.slice(0).reverse().map((item, k) =>
                                                    item.status == '0'|| item.status == '5' || item.status == '6'?
                                                    <div key={k} className="col-lg-2 col-md-3 p-2">
                                                    <div className="customCard shadow p-3">
                                                        <div className="w-100 pull-left">
                                                            <div className="bookedCeleb2 radius-full pull-right">
                                                                <img src={item.app_user.thumbnail_profile_pic?item.app_user.thumbnail_profile_pic:require('../img/celebrity1.png')} alt='test' />
                                                            </div>
                                                        </div>

                                                        <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>{item.app_user.name}</b>

                                                        <div className="rupees font14 font-weight-bold pull-right redf">+₹ <span id="add">{item.price}</span></div>
                                                        <div className="w-100 font8 grey-color pull-right text-right">{item.payment_date?this.daterender(item.payment_date):'yyyy-mm-dd'}</div>
                                                    </div>
                                                </div>:null)
                                                    : <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4 text-center">
                                                        <p>
                                                            {/* user not found */}
                                                            No Transaction </p>
                                                    </div>
                                                : <Newsktn count={12} size={"15%"} sizeH={196} />

                                        }
                                        {/* <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div> */}
                                    </div>
                                </div>
                            </div>



                        {/* <div className="col-lg-10 bg-white rounded shadow-lg pb-4 pt-2 pl-4 pr-4">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PAYMENT DONE</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">PAYMENT PENDING</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>

                                
                            </div>
                            <div class="tab-pane fade " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                
                                
                                </div>
                            </div>
                        </div> */}
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
            <Loader loading={this.state.loading} />
        </div>
    );
  }
}

export default Mytrasaction;