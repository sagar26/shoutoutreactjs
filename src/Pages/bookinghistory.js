import React, { Component } from 'react';
// import Header from '../component/header';
import Footer from '../component/footer';
import {Link} from 'react-router-dom';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import StarRatings from 'react-star-ratings';
import {retrieveItem} from '../component/retriveitem';
import Toast from '../utils/toast'
import Loader from '../utils/loader'

class Bookinghistory extends Component {
  constructor(props){
    super();
    this.routeChange = this.routeChange.bind(this);
    this.state={
      muted: true,
      paused:false,
      clicks: 0,
      show:true,
      loadsearData:[],
      modalVisible:false,
      modalVisible1:false,
      setpricevisible:false,
      modalVisiblecc:false,

      // getid:this.props.match.params.id
    }
    this.getusertype();
  }
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
            currentdata:item,
            // otp: item.otp_verified,
            name: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        this.fetchbookh(this.state.token)
        this.notification(this.state.id,this.state.token);
        // this.fetchreview(this.state.selectid,this.state.token);
        // this.fetchfollow(this.state.selectid, this.state.token)
        // alert(this.state.getid)

    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
    
    // alert('Welcome, '+this.state.name)
    // alert(this.state.getid)
    
}
fetchbookh=async(token)=>{
  this.setState({loading: true});
    var url=constant.SER_URL+'/booking-history';
  await fetch(url,{
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        }
    })
        .then((response) => response.json())
        .then((response) =>{
          var resmsg = response['code'];
          if (resmsg == "200"){
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            this.setState({
              resdata:response['data'],
            })
            setTimeout(() => {
              this.setState({
                  loadsk:true
              })
          }, 1000);

            console.log(JSON.stringify(response['data']));

          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);

            console.log(JSON.stringify(response['data']));
          }
        }).catch(error => {
          console.log('error: '+error);
        this.setState({
            loading: false
        });
    });
}
openmodel=(visible,param)=>{
  if(param == 'pending'){
      this.setState({
          modalVisible: visible,
      })

  }else{
      this.setState({
          modalVisible1: visible,
      })
  }

}
closemodel=(visible,param)=>{
  console.log('visible',visible)
  console.log('param',param)
  if(visible == 'pending'){
    this.setState({
        modalVisible: false,
    })

}else{
    this.setState({
        modalVisible1: false,
    })
}
}


  routeChange(){
    let path = "/historydone";
    this.props.history.push(path);
  }

  gotopage=(id,id1)=>{
    console.log(id +'..n..'+id1)
    this.fetchcele(id)
  }
  fetchcele=async(id)=>{
    window.location.href = "/booking/"+id;
    console.log(id)
  }

  statusbutton=(key)=>{
    switch (key) {
        case 0:
        return 'pending'
      break;
      case 1:
        return 'pending'
      break;
      case 2:
          return 'done'
      break;
      case 3:
            return 'done'
      break;
      case 4:
            return 're-uploading'
      break;
      case 5:
            return 'rejected'
      break;
      case 6:
            return 'rejected'
      break;
      default:
            return 'pending'
        break;
    }
  }
  // daterender=(param)=>{
  //   var date = param;
  //   var datearray = date.split("-");
  //   var newdate = datearray[2] + '/' + datearray[1] + '/' + datearray[0];
  //   // return newdate
  //   var d = new Date();
  //   var month = d.getMonth()+1;
  //   var day = d.getDate();
  //   var day1 = d.getDate()-1;
  //   var output = d.getFullYear() + '-' +
  //       ((''+month).length<2 ? '0' : '') + month + '-' +
  //       ((''+day).length<2 ? '0' : '') + day;
  //       var output1 = d.getFullYear() + '-' +
  //       ((''+month).length<2 ? '0' : '') + month + '-' +
  //       ((''+day1).length<2 ? '0' : '') + day1;
  //   if(param == output){
  //       return "Today"
  //   }
  //   else if(param == output1){
  //       return "Yesterday"
  //   }
  //   else{
  //       return newdate
  //   }
  // }
  notification = async (id,token) => {
    // this.setState({loading: true});
    var url = constant.SER_URL + '/notification-list';
    await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: {
            celebrity_id: id
        }
    })
    .then((response) => response.json())
    .then((response) => {
      console.log("notification", response)
            this.setState({
                notifydata: response['data']
            })
        })
        .catch(error => console.log(error)) //to catch the errors if any
  }
  daterender=(param)=>{
    var date = param;
    var datearray1 = date.split(" ");
    var datearray = datearray1[0].split("-");
    var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
    return this.time_ago(newdate)
  }
  time_ago=(time) =>{
  
    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) time = time.getTime();
        break;
      default:
        time = +new Date();
    }
    var time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;
  
    if (seconds == 0) {
      return 'Just now'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'from now';
      list_choice = 2;
    }
    var i = 0,
      format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
      }
    return time;
  }
  statuspara=(param)=>{
        switch (param) {
          case 'like':
            return 'fa-thumbs-up'
            break;
          case 'comment':
              return 'fa-comment'
              break;
          case 'shoutout-new-request':
            return 'fa-bell'
            break;
          case 'shoutout-request-accepted':
            return 'fa-check'
            break;
          case 'shoutout-request-rejected':
            return 'fa-ban'
            break;
            case 'celebrity-approved':
            return 'account-check'
            break;
            case 'celebrity-rejected':
            return 'account-remove'
            break;
          default:
              return 'fa-thumbs-up'
            break;
        }
      }
  render() {
    return (
      <div className="body-color">
        <div className={this.state.modalVisible?"modal fade show":"modal fade"} id="myModal" style={{display:this.state.modalVisible?'block':'none',opacity:this.state.modalVisible?'1':'0'}}>
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title redf">Pending</h4>
                <button type="button" className="close" data-dismiss="modal" onClick={this.closemodel.bind(false,'pending')}>&times;</button>
              </div>
              <div className="modal-body">
                There is no shoutout.
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.closemodel.bind(false,'pending')}>Close</button>
              </div>
            </div>
          </div>
        </div>
        {
          this.state.modalVisible?
          <div class="modal-backdrop fade show"></div>:null
        }
        
        {/* <Header/> */}
        <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class={this.state.usertype != null || this.state.usertype != undefined?"nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold":"nav-link  pl-0 pr-0 text-white font-weight-bold"} href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  {
                    this.state.usertype != null || this.state.usertype != undefined?
                    <div className="dropdown-menu dropdown-menu-right">
                    <span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                      <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                      {
                        this.state.usertype == '2'?
                        <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                      }
                      
                      <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    </div>:null
                  }
                    
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.notifydata ? this.state.notifydata.length >=3?this.state.notifydata.length+'+':this.state.notifydata.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {
                                                this.state.notifydata  ?
                                                    this.state.notifydata.slice(0,5).map((item, k) =>
                                                        <div class="dropdown-item d-flex align-items-center mainnoti" key={k}>
                                                        <div class="mr-3">
                                                          <div class="icon-circle bg-primary">
                                                            <i class={"fa "+this.statuspara(item.page)+" text-white"} aria-hidden="true"></i>

                                                          </div>
                                                        </div>
                                                        <div>
                                                          <div class="small text-gray-500">{this.daterender(item.created_at)}</div>
                                                          <span>{item.message}</span>
                                                        </div>
                                                      </div>
                                                    ) : <p><center>no notification</center></p>
                                            }
                                            {
                                              this.state.notifydata ?
                                                this.state.notifydata.length > 5 ?
                                                  <center>
                                                    {/* <button className="dropdown-item text-center small text-gray-500"> */}
                                                      <Link
                                                        to="/mynotification" className="dropdown-item text-center small text-gray-500">SEE MORE</Link>
                                                        {/* </button> */}
                                                  </center>
                                                   : null : null
                                             }
                                        </div>
                                    </div>

                                </div>


            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
        <div className="container-fluid pribg-color blueContainer"></div>
        <div className="container pb-5">
          <div className="row">
            <div className="container">
              <div className="col-lg-12 pl-0"><h5 className="font-weight-bold mb-4 text-white">Booking History</h5></div>
              <div className="row">
                {
                  this.state.loadsk?
                  this.state.resdata.length>0?
                  this.state.resdata.slice(0).reverse().map((item,k)=>
                  <div className="col-lg-4 col-md-4 p-2" onClick={()=>this.statusbutton(item.status) == 'pending'?this.openmodel(true,'pending'):this.statusbutton(item.status) == 'rejected'?alert('rejected'):this.gotopage(item.app_user_1.id,item.id)}>
                  <div className="card p-3">
                    <div className="w-100">
                      <div className="bookedCeleb radius-full float-left">
                        <img src={require('../img/celebrity1.png')} alt='test' />
                      </div>
                      <div className="idmain float-right">
                        <div className="w-100 d-flex align-items-center justify-content-between font9 text-capitalize"><b className="mr-2 redf">{this.statusbutton(item.status) == 'pending'?'pending':this.statusbutton(item.status) == 'rejected'?"rejected":this.statusbutton(item.status) == 're-uploading'?"re-uploading":'done'}</b><i className="fa fa-chevron-right grey-color"></i></div>
                        <div className="w-100 font8 float-left font-weight-bold">ID:{item.id}</div>
                      </div>
                    </div>
                    <b className="w-100 font10 mt-2">{item.app_user_1.celebrity_name?item.app_user_1.celebrity_name:'from name'}</b>
                    {
                      item.addressed_to?
                      <i className="w-100 font9"><i className="grey-color mr-1">for</i><b>{item.addressed_to?item.addressed_to:'from name'}</b></i>
                      :<i className="w-100 font9"><i className="grey-color mr-1"></i></i>
                    }
                    
                    <div className="grey-color font8 mt-3"><i class="fa fa-calendar mr-2"></i>{item.shoutout_date?this.daterender(item.shoutout_date):'yyyy-mm-dd'}</div>
                  </div>
                </div>)
                :<div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4 text-center">
                <p>
                  {/* user not found */}
                  Data not found
                </p>
              </div>
                :<Newsktn count={5} size={"30%"} sizeH={172} />

                }

                
               
              </div>
            </div>
          </div>
        </div>
    <Footer/>
    {/* <Loader loading={this.state.loading} /> */}
      </div>
    );
  }
}

export default Bookinghistory;