import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
// import ReactPlayer from 'react-player';
import {retrieveItem} from '../component/retriveitem';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import Loader from '../utils/loader'
import Toast from '../utils/toast'
import $ from 'jquery'
import {
    Link
  } from 'react-router-dom'

class Updateprofile extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
        celename:'',
        celeprice:'',
        celemail:'',
        celedob:'',
        celemobile:''
      }
      this.getusertype();
  }
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
          currentdata:item,
            otp: item.otp_verified,
            name: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        // if(this.state.usertype == '2'){
            this.fetchcele(this.state.id, this.state.token)
            this.fetchaccount(this.state.token)
        // }
        this.notification(this.state.id,this.state.token);
    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
  //   // this.refs.toast.show('Welcome, '+this.state.name, Toasty);
    // alert('Welcome, '+this.state.name)
   
}
fetchcele = async (id, token) => {
    // this.setState({ loading: true });
    var url = constant.SER_URL + '/celebrity-profile';
    console.log('url '+url)
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
      body: JSON.stringify({
        celebrity_id: id,
        platform: 'web',
      })
    })

      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        if (resmsg == "200") {
          // this.setState({ loading: true });
          this.setState({loadsk:false});
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          this.setState({
            celedata: response['data'],
            celeid: response['data']['id'],
            celename: response['data']['name'],
            celeprice: response['data']['price'],
            celemail:response['data']['email'],
            celedob:response['data']['dob'],
            celemobile:response['data']['mobile'],
            celecategory: response['data']['categories'],
            celepp: response['data']['profile_pic'],
            celetpp: response['data']['thumbnail_profile_pic'],
            celepv: response['data']['profile_video'],
            celedescription: response['data']['description'],
            celetags: response['data']['tags'],
            reviewccount: response['data']['average_rating'],
            celerating1: response['data']['celebrity_ratings'],
            celereview1: response['data']['celebrity_ratings'],
            shoutlist: response['data']['shoutouts_1'],
            // shoutlistname:response['data']['shoutouts_1'].app_user.customer_name,
            // shoutlistmedia:response['data']['shoutouts_1']['shoutout_media']['path'],
            // celerating:this.state.celerating1[this.state.celerating1.length-1].rating,
            // celereview:this.state.celereview1[this.state.celereview1.length-1].review,
          })
          console.log(response['data']);
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error);
        this.setState({
          loading: false
        });
      });
    
  }
  fetchaccount = async (token) => {
    // this.setState({ loading: true });
    var url = constant.SER_URL + '/get-celebrity-account-details';
    console.log('url '+url)
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
    //   body: JSON.stringify({
    //     celebrity_id: id,
    //     platform: 'web',
    //   })
    })

      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        if (resmsg == "200") {
          // this.setState({ loading: true });
          this.setState({loadsk:false});
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          this.setState({
            getdata: response['data'],
            getname: response['data']['name'],
            getmobileno: response['data']['mobile_no'],
            getaccountno: response['data']['account_no'],
            getifsccode: response['data']['ifsc_code'],
          })
          console.log(JSON.stringify(response['data']));
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error);
        this.setState({
          loading: false
        });
      });
    
  }
photoUpload = e =>{
    e.preventDefault();
    const reader = new FileReader();
    const file = e.target.files[0];
    reader.onloadend = () => {
      this.setState({
        file: file,
        celetpp: reader.result
      });
    }
    reader.readAsDataURL(file);
    console.log("file",reader)
}
photoUpload1=()=>{
    $('#imageurl').click()
    // alert('asa')
}
notification = async (id,token) => {
  // this.setState({loading: true});
  var url = constant.SER_URL + '/notification-list';
  await fetch(url, {
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          authorization:'Bearer'+token,
      },
      body: {
          celebrity_id: id
      }
  })
  .then((response) => response.json())
  .then((response) => {
    console.log("notification", response)
          this.setState({
              notifydata: response['data']
          })
      })
      .catch(error => console.log(error)) //to catch the errors if any
}
daterender=(param)=>{
  var date = param;
  var datearray1 = date.split(" ");
  var datearray = datearray1[0].split("-");
  var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
  return this.time_ago(newdate)
}
time_ago=(time) =>{

  switch (typeof time) {
    case 'number':
      break;
    case 'string':
      time = +new Date(time);
      break;
    case 'object':
      if (time.constructor === Date) time = time.getTime();
      break;
    default:
      time = +new Date();
  }
  var time_formats = [
    [60, 'seconds', 1], // 60
    [120, '1 minute ago', '1 minute from now'], // 60*2
    [3600, 'minutes', 60], // 60*60, 60
    [7200, '1 hour ago', '1 hour from now'], // 60*60*2
    [86400, 'hours', 3600], // 60*60*24, 60*60
    [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
    [604800, 'days', 86400], // 60*60*24*7, 60*60*24
    [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
    [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
    [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
    [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
    [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
    [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
    [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
    [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
  ];
  var seconds = (+new Date() - time) / 1000,
    token = 'ago',
    list_choice = 1;

  if (seconds == 0) {
    return 'Just now'
  }
  if (seconds < 0) {
    seconds = Math.abs(seconds);
    token = 'from now';
    list_choice = 2;
  }
  var i = 0,
    format;
  while (format = time_formats[i++])
    if (seconds < format[0]) {
      if (typeof format[2] == 'string')
        return format[list_choice];
      else
        return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
    }
  return time;
}
statuspara=(param)=>{
  switch (param) {
    case 'like':
      return 'fa-thumbs-up'
      break;
    case 'comment':
        return 'fa-comment'
        break;
    case 'shoutout-new-request':
      return 'fa-bell'
      break;
    case 'shoutout-request-accepted':
      return 'fa-check'
      break;
    case 'shoutout-request-rejected':
      return 'fa-ban'
      break;
    default:
        return 'fa-thumbs-up'
      break;
  }
}
handlecelename=(e)=>{
  this.setState({ fulname: e.target.value })
}
handlemail=(e)=>{
  this.setState({ femail: e.target.value })
}
handledob=(e)=>{
  this.setState({ fDOB: e.target.value })
}
// handlecelename=(e)=>{
//   this.setState({ imageuri: e.target.value })
// }
handlemo=(e)=>{
  this.setState({ celemobile: e.target.value })
}
handledes=(e)=>{
  this.setState({ celedescription: e.target.value })
}
signup=()=>{
  // this.handlecelename()
  // this.handlemail()
  // this.handledob()
  // this.handlemo()
  // this.handledes()
    let token = this.state.token
    var timeNow = new Date().getTime();
    var mediaId = timeNow + this.state.id;
var body ={name:this.state.fulname,email:this.state.femail,dob:this.state.fDOB,image:this.state.celetpp,description:this.state.celedescription}
    // this.setState({loading: true});
    const formData = new FormData();
    formData.append("name", this.state.fulname);
    formData.append("email", this.state.femail);
    // formData.append("gender", this.state.fgender);
    formData.append("dob",this.state.fDOB);
    // formData.append("image", this.state.imageuri);
    formData.append("image", this.state.celetpp);
    
    if(this.state.usertype == '1'){
      formData.append("description",'customer user');
      // formData.append("category", "[1,2]");
    }else{
      formData.append("description",this.state.celedescription);
      // formData.append("category", JSON.parse("\"["+this.state.newcaty+"]\""));
    }
    
    
    console.log(body)
    var url=constant.SER_URL+'/update-profile';
    console.log("URL", url)
  //  fetch(url,{
  //       method:'POST',
  //       headers: {
  //           Accept: 'application/json',
  //           'Content-Type': 'multipart/form-data',
  //           // 'Content-Type': 'application/json',
  //           authorization:'Bearer'+token,

  //       },
  //       // body:JSON.stringify(body)
  //       body:formData
  //   })
  //       .then((response) => response.json())
  //       .then((response) =>{
  //         var resmsg = response['code'];
  //         alert('Code: '+resmsg)
  //         console.log("Update", JSON.stringify(response));
  //         console.log("Updating...");
  //         if (resmsg == "200"){
  //           this.setState({loading: true});
  //           setTimeout(() => { this.setState({loading: false}); }, 1000);
  //           console.log("Update");
  //               // this.fetchcele(this.state.id,this.state.token);
  //               // this.props.navigation.navigate('ProfileScreen')
  //           }
  //           else if (resmsg == "400") {
  //             this.setState({loading: true});
  //             setTimeout(() => { this.setState({loading: false}); }, 1000);
  //             console.log(JSON.stringify(response['message']));
  //           }
  //           else if (resmsg == "422") {
  //             this.setState({loading: true});
  //             setTimeout(() => { this.setState({loading: false}); }, 1000);
  //             console.log(JSON.stringify(response['message']));

  //         }
  //         else {
  //           console.log('Please fill register form');
  //           }

  //           this.setState({
  //             loading: false
  //         });
  //       }).catch(error => {
  //         // this.props.navigation.navigate('ProfileScreen')
  //         // alert('err'+JSON.stringify(error))
  //         alert('Server Error')
  //         // this.props.navigation.navigate('ProfileScreen')
  //       this.setState({
  //           loading: false
  //       });
  //   });
}

  render() {
    const {celetpp, 
        name, 
        status, 
        active} = this.state;
    return (
        <div>
        {/* <Header/> */}
        <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class={this.state.usertype != null || this.state.usertype != undefined?"nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold":"nav-link  pl-0 pr-0 text-white font-weight-bold"} href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  {
                    this.state.usertype != null || this.state.usertype != undefined?
                    <div className="dropdown-menu dropdown-menu-right">
                    <span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                      <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                      {
                        this.state.usertype == '2'?
                        <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                      }
                      
                      <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    </div>:null
                  }
                    
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.notifydata ? this.state.notifydata.length >=3?this.state.notifydata.length+'+':this.state.notifydata.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {
                                                this.state.notifydata  ?
                                                    this.state.notifydata.slice(0,5).map((item, k) =>
                                                        <div class="dropdown-item d-flex align-items-center mainnoti" key={k}>
                                                        <div class="mr-3">
                                                          <div class="icon-circle bg-primary">
                                                            <i class={"fa "+this.statuspara(item.page)+" text-white"} aria-hidden="true"></i>

                                                          </div>
                                                        </div>
                                                        <div>
                                                          <div class="small text-gray-500">{this.daterender(item.created_at)}</div>
                                                          <span>{item.message}</span>
                                                        </div>
                                                      </div>
                                                    ) : <p><center>no notification</center></p>
                                            }
                                            {
                                              this.state.notifydata ?
                                                this.state.notifydata.length > 5 ?
                                                  <center>
                                                    {/* <button className="dropdown-item text-center small text-gray-500"> */}
                                                      <Link
                                                        to="/mynotification" className="dropdown-item text-center small text-gray-500">SEE MORE</Link>
                                                        {/* </button> */}
                                                  </center>
                                                   : null : null
                                             }
                                        </div>
                                    </div>

                                </div>


            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row justify-content-center">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="updatepik mb-4 shadow mr-2">
                                <img src={celetpp?celetpp:require('../img/pro-default.png')} alt='test' />
                                <div className="camera fimage">
                                    <div class="buttonimh" id="setimage" onClick={(e)=>this.photoUpload1()}>
                                    <span class="label-up"><i class="fa fa-camera" aria-hidden="true"></i></span>
                                    <span class="label-up"><i class="fa fa-upload" aria-hidden="true"></i></span>
                                    </div>
                                    <input type="file" id="imageurl" name="imageurl" style={{display:"none"}} onChange={(e)=>this.photoUpload(e)}/>
                                </div>
                            </div>
                            <div className="col-md-9">
                                <h3 className="blackWhite text-capitalize"><b>{this.state.name?this.state.name:'Sagar Mistry'}</b></h3>
                                <p className="blackWhite">
                                    {
                                        this.state.celecategory ?
                                            <div className="d-flex true flex-wrap">
                                                {
                                                    this.state.celecategory ?
                                                        this.state.celecategory.map((items, i) => <p className="w-auto blackWhite text-uppercase prl mb-0" key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)
                                                        : <p className="w-auto blackWhite text-uppercase mb-0">uncategorized</p>
                                                }
                                            </div> :
                                            null
                                    }
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-10 bg-white rounded shadow-lg p-4">
                        <div className="row">
                            <div className="w-100 d-flex justify-content-center flex-wrap">

                                <div className="col-md-12 mb-2 d-flex align-items-center">
                                    <h6 className="w-100 font-weight-bold mb-0">Update profile</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder={this.state.celename?this.state.celename:"First Name"} name="text1" value={this.state.celename?this.state.celename:null} onChange={this.handlecelename}/>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder={this.state.celedob?this.state.celedob:"DOB"} name="text1" value={this.state.celedob?this.state.celedob:null} onChange={this.handledob}/>
                                    </div>
                                </div>
                                {
                                  this.state.usertype == '1'?null:
                                  <div className="col-md-12">
                                    <div class="form-group w-100">
                                        <textarea type="text" class="form-control" placeholder={this.state.celedescription?this.state.celedescription:"Description"} name="text1" value={this.state.celedescription?this.state.celedescription:null} onChange={this.handledes}/>
                                    </div>
                                </div>
                                }
                                


                                {/* <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Update mobile</h6>
                                </div> */}
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Enter mobile number" name="text1" value={this.state.celemobile?this.state.celemobile:null} onChange={this.handlemo}/>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Enter email address" name="text1" value={this.state.celemail?this.state.celemail:null} onChange={this.handlemail}/>
                                    </div>
                                </div>
                                {/* <div className="col-md-2">
                                    <div class="form-group w-100">
                                        <button className="btn btn-primary pull-right">SEND OTP</button>
                                    </div>
                                </div> */}

{
  this.state.usertype == '1'?null:
  <div>
    <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Enter bank details</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder={this.state.getaccountno?this.state.getaccountno:"Enter bank Ac/ number"} name="text1" value={this.state.getaccountno?this.state.getaccountno:null} onChange={this.handleacc}/>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder={this.state.getifsccode?this.state.getifsccode:"Enter IFSC code"} name="text1" value={this.state.getifsccode?this.state.getifsccode:null} onChange={this.handleifsc}/>
                                    </div>
                                </div>


                                <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Set booking price</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-12">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder={this.state.celeprice?this.state.celeprice:"Your booking price"} name="text1"  value={this.state.celeprice?this.state.celeprice:null} onChange={this.handlebp}/>
                                    </div>
                                </div>
  </div>
}
                                

                                <div class="form-group w-100 mt-3 text-center">
                                    <button class="btn btn-lg btn-primary" onClick={()=>this.signup()}> SAVE DETAILS</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Updateprofile;