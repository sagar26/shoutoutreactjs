import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Follow extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row justify-content-center">
                        
                        <div className="col-lg-6 bg-white rounded shadow-lg pb-4 pt-2 pl-4 pr-4">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home1" role="tab" aria-controls="nav-home" aria-selected="true">FOLLOWING</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile1" role="tab" aria-controls="nav-profile" aria-selected="false">FOLLOWERS</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home1" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">UNFOLLOW</button>
                                    </div>
                                </div>
                                <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">UNFOLLOW</button>
                                    </div>
                                </div>
                                <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">UNFOLLOW</button>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="tab-pane fade" id="nav-profile1" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">REMOVE</button>
                                    </div>
                                </div>
                                <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">REMOVE</button>
                                    </div>
                                </div>
                                <div className="w-100 pt-3 pb-1 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain2 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-50 font8 float-left grey-color">Actor</div>
                                        <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">REMOVE</button>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Follow;