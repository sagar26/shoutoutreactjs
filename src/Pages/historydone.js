import React, { Component } from 'react';
// import Header from '../component/header';
import Footer from '../component/footer';
import ReactPlayer from 'react-player';
import {Link} from 'react-router-dom';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import StarRatings from 'react-star-ratings';
import {retrieveItem} from '../component/retriveitem';
import Toast from '../utils/toast'
import Loader from '../utils/loader'
// import { async } from 'q';
import $ from 'jquery'

class Historydone extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
        loadsearData:[]
        // getid:this.props.match.params.id
      }
      this.getusertype();
  }
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
            currentdata:item,
            // otp: item.otp_verified,
            name: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        let id = this.props.match.params.id
        let ids = this.props.match.params.ids
        
        console.log(id+'.-.'+ids);
        this.setState({
          selectid:id,
          selectids:ids
        })
        this.fetchcele(this.state.selectid, this.state.token)
        this.notification(this.state.id,this.state.token);
        // this.fetchreview(this.state.selectid,this.state.token);
        // this.fetchfollow(this.state.selectid, this.state.token)
        // alert(this.state.getid)

    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
    
    // alert('Welcome, '+this.state.name)
    // alert(this.state.getid)
    
}
// fetchcele = async (id, token) => {
//   this.setState({ loading: true });
//   var url = constant.SER_URL + '/celebrity-profile';
//   console.log('url '+url)
//   await fetch(url, {
//     method: 'POST',
//     headers: {
//       'Accept': 'application/json',
//       'Content-Type': 'application/json',
//       authorization: 'Bearer' + token,
//     },
//     body: JSON.stringify({
//       celebrity_id: id,
//     })
//   })

//     .then((response) => response.json())
//     .then((response) => {
//       var resmsg = response['code'];
//       if (resmsg == "200") {
//         this.setState({ loading: true });
//         setTimeout(() => { this.setState({ loading: false }); }, 2000);
//         this.setState({
//           celedata: response['data'],
//           celeid: response['data']['id'],
//           celename: response['data']['name'],
//           celeprice: response['data']['price'],
//           celecategory: response['data']['categories'],
//           celepp: response['data']['profile_pic'],
//           celetpp: response['data']['thumbnail_profile_pic'],
//           celepv: response['data']['profile_video'],
//           celedescription: response['data']['description'],
//           celetags: response['data']['tags'],
//           reviewccount: response['data']['average_rating'],
//           celerating1: response['data']['celebrity_ratings'],
//           celereview1: response['data']['celebrity_ratings'],
//           shoutlist: response['data']['shoutouts_1'],
//           getsocialmedia:response['data']['shoutouts_1'][0]['shoutout_media'][0].path,
//           shoutlistname:response['data']['shoutouts_1'][0]['app_user']['customer_name'],
//           // shoutlistmedia:response['data']['shoutouts_1']['shoutout_media']['path'],
//           // celerating:this.state.celerating1[this.state.celerating1.length-1].rating,
//           // celereview:this.state.celereview1[this.state.celereview1.length-1].review,
//         })
//         console.log(response['data']);
//         // // alert('200')
//         // console.log(this.state.crating1);
//         // alert(this.state.shoutlist.length);
//         // console.log(this.state.getsocialmedia);
//         const newData =  this.state.shoutlist.filter(item=>{
//           const itemData = `${item.id}`;
//           return itemData.indexOf(this.state.selectid) > -1;
//         })
//         this.setState({ loadsearData: newData,countlisty:newData.length });
//         console.log("newData : ",this.state.loadsearData)
//         console.log("this.state.loadsearData",this.state.loadsearData.shoutout_media[0].path)
//         this.setState({
//           sharepath:this.state.loadsearData.shoutout_media[0].path
//         })
//         this.fetchlikes(this.state.loadsearData.id, this.state.token)
//         this.fetchcomment(this.state.loadsearData.id, this.state.token)
//       }
//       else if (resmsg == "400") {
//         this.setState({ loading: true });
//         setTimeout(() => { this.setState({ loading: false }); }, 2000);
//         console.log(JSON.stringify(response['message']));
//       }
//       else if (resmsg == "422") {
//         this.setState({ loading: true });
//         setTimeout(() => { this.setState({ loading: false }); }, 2000);
//         console.log(JSON.stringify(response['data']));
//       }
//     }).catch(error => {
//       console.log('error: ' + error);
//       this.setState({
//         loading: false
//       });
//     });
// }
fetchcele = async (id, token) => {
  // this.setState({ loading: true });
  var url = constant.SER_URL + '/celebrity-all-shoutouts';
  console.log('url '+url)
  await fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      authorization: 'Bearer' + token,
    },
    body: JSON.stringify({
      celebrity_id: id,
    })
  })

    .then((response) => response.json())
    .then((response) => {
      var resmsg = response['code'];
      if (resmsg == "200") {
        // this.setState({ loading: true });
        
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        this.setState({
          loadsearData1: response['data'],
          countlisty:response['data'].length
          // shoutlistmedia:response['data']['shoutouts_1']['shoutout_media']['path'],
          // celerating:this.state.celerating1[this.state.celerating1.length-1].rating,
          // celereview:this.state.celereview1[this.state.celereview1.length-1].review,
        })
        console.log(response['data']);
        const newData = this.state.loadsearData1.filter(item => {
          const itemData = `${item.id}`;
          return itemData.indexOf(this.state.selectids) > -1;
        })
        this.setState({ loadsearData: newData,countlist:newData.length});
        console.log("newData : ",this.state.loadsearData)
        console.log("this.state.loadsearData",this.state.loadsearData[0].shoutout_media[0])
        if(this.state.loadsearData[0].shoutout_media[0]){
          this.setState({
            sharepath:this.state.loadsearData[0].shoutout_media[0].path
          })
        }
        this.fetchlikes(this.state.loadsearData[0].id, this.state.token)
        this.fetchcomment(this.state.loadsearData[0].id, this.state.token)
      }
      else if (resmsg == "400") {
        this.setState({ loading: true });
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        console.log(JSON.stringify(response['message']));
      }
      else if (resmsg == "422") {
        this.setState({ loading: true });
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        console.log(JSON.stringify(response['data']));
      }
    }).catch(error => {
      console.log('error: ' + error);
      this.setState({
        loading: false
      });
    });
}
fetchlikes = async (id, token) => {
  // this.setState({ loading: true });
  var url = constant.SER_URL + '/shoutout-like-list';
  console.log('url '+url)
  await fetch(url, {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      authorization: 'Bearer' + token,
    },
    body: JSON.stringify({
      shoutout_id: id,
    })
  })

    .then((response) => response.json())
    .then((response) => {
      var resmsg = response['code'];
      if (resmsg == "200") {
        // this.setState({ loading: true });
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        this.setState({
          likesdata: response['data']['likes'],
        })
        console.log(response['data']['likes']);
      }
      else if (resmsg == "400") {
        this.setState({ loading: true });
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        console.log(JSON.stringify(response['message']));
      }
      else if (resmsg == "422") {
        this.setState({ loading: true });
        setTimeout(() => { this.setState({ loading: false }); }, 2000);
        console.log(JSON.stringify(response['data']));
      }
    }).catch(error => {
      console.log('error: ' + error);
      this.setState({
        loading: false
      });
    });
}
routelike=()=>{
  // alert('helloworld')
  this.like(this.state.loadsearData[0].id, this.state.selectid)
}
like=(id,celeid)=>{
  let token = this.state.token
  let body={
      shoutout_id:id,
      celebrity_id:celeid
  }
  console.log(body);
  this.setState({loading: true});

  var url=constant.SER_URL+'/like-shoutout';
  console.log("URL", url)
  fetch(url,{
      method:'POST',
      headers: {
          Accept: 'application/json',
          // 'Content-Type': 'multipart/form-data',
          'Content-Type': 'application/json',
          authorization:'Bearer'+token,

      },
      body:JSON.stringify(body)
      // body:formData
  })
      .then((response) => response.json())
      .then((response) =>{
        var resmsg = response['code'];
      //   alert(resmsg)
        if (resmsg == "200"){
          // this.setState({loading: true});
          setTimeout(() => { this.setState({loading: false}); }, 1000);
          // this.setState({likeicon:!this.state.likeicon})
              // alert("Success");
              window.location.reload();
              // this.fetchcele(this.state.id,this.state.token);
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));

        }
        else {
          console.log('Oops server issue');
          }

          this.setState({
            loading: false
        });
      }).catch(error => {
        alert('err',error)
      this.setState({
          loading: false
      });
  });
}
fetchcomment=async(id,token)=>{
  var url=constant.SER_URL+'/shoutout-comment-list';
  console.log("URL", url)
 await fetch(url,{
      method:'POST',
      headers: {
          Accept: 'application/json',
          // 'Content-Type': 'multipart/form-data',
          'Content-Type': 'application/json',
          authorization:'Bearer'+token,

      },
      body: JSON.stringify({
          shoutout_id:id,
      })
  })

      .then((response) => response.json())
      .then((response) =>{
        var resmsg = response['code'];
        if (resmsg == "200"){
          this.setState({loadsk:false});
          setTimeout(() => { this.setState({loading: false}); }, 1000);
          this.setState({
              commentlisty:response['data']['comments'],
              loadsk:true
          })
          console.log('Comment list: ',response['data']['comments'])
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));

        }
        else {
          console.log('Please fill register form');
          }

          this.setState({
            loading: false
        });
      }).catch(error => {
        alert('err',error)
      this.setState({
          loading: false
      });
  });
}
handlecomment=(e)=>{
  this.setState({ commentvalue: e.target.value })
  // this.handleSubmit(this.state.loadsearData.id, this.state.celeid)
}
  handleSubmit = (event) => {
    this.commentinsert(this.state.loadsearData[0].id, this.state.selectid)
    event.preventDefault();
  }
  commentinsert=async(id, celeid)=>{
    let token = this.state.token
    let commentext = this.state.commentvalue
    let body = {
      shoutout_id: id,
      celebrity_id: celeid,
      comment: commentext
    }
    console.log('submit comment: ',body)
    this.setState({loading: true});
      var url=constant.SER_URL+'/comment-shoutout';
      console.log(body)
      console.log("URL", url)
     await fetch(url,{
          method:'POST',
          headers: {
              Accept: 'application/json',
              // 'Content-Type': 'multipart/form-data',
              'Content-Type': 'application/json',
              authorization:'Bearer'+token,

          },
          body:JSON.stringify(body)
          // body:formData
      })

          .then((response) => response.json())
          .then((response) =>{
            var resmsg = response['code'];
            // alert('Code: ',resmsgs)
            if (resmsg == "200"){
              
              setTimeout(() => { this.setState({loading: false}); }, 1000);
                  console.log("Update");
                  console.log(response['data'])
                  window.location.reload();
                  // this.fetchcele(this.state.i1d,this.state.token);
                  // this.props.navigation.navigate('ProfileScreen')
                  // this.fetchcomment(this.state.ids,this.state.token)
              }
              else if (resmsg == "400") {
                this.setState({loading: true});
                setTimeout(() => { this.setState({loading: false}); }, 1000);
              console.log(JSON.stringify(response['message']));
              }
              else if (resmsg == "422") {
                this.setState({loading: true});
                setTimeout(() => { this.setState({loading: false}); }, 1000);
                console.log(JSON.stringify(response['message']));

            }
            else {
              console.log('Please fill register form');
              }

              this.setState({
                loading: false
            });
          }).catch(error => {
            alert('err',error)
          this.setState({
              loading: false
          });
      });
  }
  daterender=(param)=>{
    var date = param;
    var datearray1 = date.split(" ");
    var datearray = datearray1[0].split("-");
    // var newdate = datearray[2] + '-' + datearray[1] + '-' + datearray[0];
    var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
    // return newdate
    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();
    var day1 = d.getDate()-1;
    var output = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day).length<2 ? '0' : '') + day;
        var output1 = d.getFullYear() + '-' +
        ((''+month).length<2 ? '0' : '') + month + '-' +
        ((''+day1).length<2 ? '0' : '') + day1;
    if(newdate == output){
        return "Today"
    }
    else if(newdate == output1){
        return "Yesterday"
    }
    else{
        return newdate
        // return "This Month"
    }
  }
  downloadurl=()=>{
    // window.location.href= this.state.sharepath?this.state.sharepath:null
    
    if(this.state.sharepath){
      window.open(this.state.sharepath?this.state.sharepath:null)
    }

  }
  notification = async (id,token) => {
    // this.setState({loading: true});
    var url = constant.SER_URL + '/notification-list';
    await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: {
            celebrity_id: id
        }
    })
    .then((response) => response.json())
    .then((response) => {
      console.log("notification", response)
            this.setState({
                notifydata: response['data']
            })
        })
        .catch(error => console.log(error)) //to catch the errors if any
  }
  daterender=(param)=>{
    var date = param;
    var datearray1 = date.split(" ");
    var datearray = datearray1[0].split("-");
    var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
    return this.time_ago(newdate)
  }
  time_ago=(time) =>{
  
    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) time = time.getTime();
        break;
      default:
        time = +new Date();
    }
    var time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;
  
    if (seconds == 0) {
      return 'Just now'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'from now';
      list_choice = 2;
    }
    var i = 0,
      format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
      }
    return time;
  }
  statuspara=(param)=>{
        switch (param) {
          case 'like':
            return 'fa-thumbs-up'
            break;
          case 'comment':
              return 'fa-comment'
              break;
          case 'shoutout-new-request':
            return 'fa-bell'
            break;
          case 'shoutout-request-accepted':
            return 'fa-check'
            break;
          case 'shoutout-request-rejected':
            return 'fa-ban'
            break;
            case 'celebrity-approved':
            return 'account-check'
            break;
            case 'celebrity-rejected':
            return 'account-remove'
            break;
          default:
              return 'fa-thumbs-up'
            break;
        }
      }
  render() {
    const formatCash = n => {
      if (n < 1e3) return n;
      if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
      if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
      if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + "B";
      if (n >= 1e12) return +(n / 1e12).toFixed(1) + "T";
    };
    return (
      <div>
        
        {/* <Header/> */}
        <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class={this.state.usertype != null || this.state.usertype != undefined?"nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold":"nav-link  pl-0 pr-0 text-white font-weight-bold"} href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  {
                    this.state.usertype != null || this.state.usertype != undefined?
                    <div className="dropdown-menu dropdown-menu-right">
                    <span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                      <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                      {
                        this.state.usertype == '2'?
                        <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                      }
                      
                      <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    </div>:null
                  }
                    
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.notifydata ? this.state.notifydata.length >=3?this.state.notifydata.length+'+':this.state.notifydata.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {
                                                this.state.notifydata  ?
                                                    this.state.notifydata.slice(0,5).map((item, k) =>
                                                        <div class="dropdown-item d-flex align-items-center mainnoti" key={k}>
                                                        <div class="mr-3">
                                                          <div class="icon-circle bg-primary">
                                                            <i class={"fa "+this.statuspara(item.page)+" text-white"} aria-hidden="true"></i>

                                                          </div>
                                                        </div>
                                                        <div>
                                                          <div class="small text-gray-500">{this.daterender(item.created_at)}</div>
                                                          <span>{item.message}</span>
                                                        </div>
                                                      </div>
                                                    ) : <p><center>no notification</center></p>
                                            }
                                            {
                                              this.state.notifydata ?
                                                this.state.notifydata.length > 5 ?
                                                  <center>
                                                    {/* <button className="dropdown-item text-center small text-gray-500"> */}
                                                      <Link
                                                        to="/mynotification" className="dropdown-item text-center small text-gray-500">SEE MORE</Link>
                                                        {/* </button> */}
                                                  </center>
                                                   : null : null
                                             }
                                        </div>
                                    </div>

                                </div>


            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
        <div className="container-fluid pribg-color blueContainer"></div>
        <div className="container pb-3 border-bottom">
          <div className="row">
            <div className="container">
              <div className="col-lg-12 pl-0"><h5 className="font-weight-bold mb-4 text-white">Booking History</h5></div>
              {
                this.state.countlist>=1?
                this.state.loadsearData?
                <div className="row justify-content-center">

                <div className="col-lg-4 col-sm-4 col-md-6 mb-5 d-flex justify-content-center">
                  <div className={this.state.paused ? "ReactPlayerMain hiden" : "ReactPlayerMain"}>
                    <button onClick={() => this.setState({ paused: !this.state.paused })} className="btn playbutton bg-transparent">
                      <i class={this.state.paused ? "fa fa-pause rubberBand animated font30 text-white" : "fa fa-play font30 text-white"}></i>
                    </button>
                    {/* <ReactPlayer className="ReactPlayerDone" url={require('../img/klippyvideo.mp4')} loop playing={this.state.paused} /> */}
                    {
                      this.state.loadsearData[0].shoutout_media[0]?
                      <ReactPlayer className="ReactPlayerDone preview" url={this.state.loadsearData[0].shoutout_media[0].path? this.state.loadsearData[0].shoutout_media[0].path: require('../img/klippyvideo.mp4')} playing={this.state.paused} loop muted={this.state.muted} />:
                      <div className="ReactPlayerDone preview"><img data-src={require('../img/novideo.png')} alt='no video found' className='lazy-img novideo' src={require('../img/novideo.png')}/></div>
                    }
                  </div>
                </div>

                <div className="col-lg-4 col-sm-4 col-md-6">
                  <h3 className="font-weight-bold blackWhite">for {this.state.loadsearData[0].app_user.customer_name?this.state.loadsearData[0].app_user.customer_name:'sagar'}</h3>
                  <div className="social-button mt-5 mb-3">
                    <button type="button" class="btn btn-sm btn-outline-dark mr-3" id='share' disabled={this.state.loadsearData[0].shoutout_media[0]?false:true}>SHARE</button>
                    {/* <a class={this.state.loadsearData[0].shoutout_media[0] != ''?"btn btn-sm btn-outline-dark atdisabled":"btn btn-sm btn-outline-dark"} href={this.state.sharepath?this.state.sharepath:"#"} download target='_blank'>DOWNLOAD</a> */}
                    <button type="button" class="btn btn-sm btn-outline-dark mr-3" id='download' onClick={this.downloadurl} disabled={this.state.loadsearData[0].shoutout_media[0]?false:true}>DOWNLOAD</button>
                    <div className="social-button mt-3 mb-3">
                      {/* {this.like(this.state.loadsearData.id, this.state.celeid)} */}
                      <button type="button" class="btn btn-sm btn-outline-dark" onClick={(k) => this.routelike()} disabled={this.state.loadsearData[0].shoutout_media[0]?false:true}>LIKE</button> <b>{this.state.likesdata?formatCash(this.state.likesdata.length):'0'}</b>
                    </div>
                  </div>
                  {/* <div className="w-100 mt-3 border-top pt-3">
                                    <button className="btn font-weight-bold blackbg radius-full text-white">
                                        REQUEST SACHIN TENDULKER</button>
                                </div> */}
                  <form className="mt-3"  onSubmit={this.handleSubmit}>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Write Comments" name="text1"  onChange={this.handlecomment}/>
                    </div>
                    <div>
                      {
                        this.state.loadsk?
                        this.state.commentlisty?
                        this.state.commentlisty.map((item,k)=>
                          <div className="w-100 mb-3 d-flex justify-content-start" key={k}>
                            <div className="bookedCeleb radius-full float-left">
                              <img src={item.app_user_1.thumbnail_profile_pic?item.app_user_1.thumbnail_profile_pic:require('../img/celebrity1.png')} alt={item.app_user_1.name?item.app_user_1.name:'sagar mistry'} />
                            </div>
                            <div className="idmain float-right">
                              <div className='head'>
                              <p className='font-weight-bold font12'>{item.app_user_1.name?item.app_user_1.name:''}</p>
                              <p >{this.daterender(item.created_at?item.created_at:'2019-10-30 15:50:08')}</p>
                              </div>
                            <p>{item.comment?item.comment:'blank'}</p>
                            </div>
                          </div>
                        )
                        :<h3>No Comments</h3>:<Newsktn count={5} size={"100%"} sizeH={52} />
                      }
                    </div>
                  </form>
                </div>
                <div className="col-lg-4 col-sm-4 col-md-6"></div>
              </div>:
                <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4 text-center">
                <p>
                  {/* user not found */}
                  Data not found
                </p>
              </div>
                :<div className="row">
                  <div className="col-lg-3 col-sm-4 col-md-6 mb-5 d-flex justify-content-center">
                  <Newsktn count={0} size={"100%"} sizeH={450} />
                  </div>
                  <div className="col-lg-7 col-sm-4 col-md-6">
                  <Newsktn count={0} size={"100%"} sizeH={450} />
                  </div>
                  <div className="col-lg-2 col-sm-4 col-md-6"></div>
                </div>
              }
              

            </div>
          </div>
        </div>
    <Footer/>
    <div class="social-share-container lnnx-modal">
    <div class="button-close">  <i class="fa fa-times font30 text-white"></i></div>
    <div class="social-buttons-container">
            <div class="social-caption">You've Come This Far...</div>
            <div class="wpbflex">
            <div class="social-button facebook">
              {/* <a href={this.state.sharepath?'https://www.facebook.com/sharer/sharer.php?u='+this.state.sharepath+'%20created%20by%20Sagar Mistry':'https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsbmshoutout.herokuapp.com&quote=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry'} target="_blank"> */}
              <a href='https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fsbmshoutout.herokuapp.com&quote=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry' target="_blank">

                {/* <span class="icon icon-sharrow"></span> */}
                <i class="fa fa-facebook font30 text-white"></i>
                <span class="social-count"></span>
              </a>
            </div>
            <div class="social-button linkedin">
                <a href="http://www.linkedin.com/shareArticle?url=http%3A%2F%2Fsbmshoutout.herokuapp.com&title=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry" target="_blank"> 
                {/* <span class="icon icon-sharrow"></span> */}
                <i class="fa fa-linkedin font30 text-white"></i>
                <span class="social-count"></span>
              </a>
            </div>
            <div class="social-button twitter">
               <a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fsbmshoutout.herokuapp.com&text=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry" target="_blank">
                 {/* <span class="icon icon-sharrow"></span> */}
                <i class="fa fa-twitter font30 text-white"></i>
                <span class="social-count"></span>
              </a>
            </div>
          <div class="social-button pinterest">
               <a href="https://pinterest.com/pin/create/bookmarklet/?media=http%3A%2F%2Fsbmshoutout.herokuapp.com%2Flnnx.png&url=http%3A%2F%2Fsbmshoutout.herokuapp.com&description=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry" target="_blank"> 
                 {/* <span class="icon icon-sharrow"></span> */}
                <i class="fa fa-pinterest font30 text-white"></i>
                <span class="social-count"></span>
              </a>
            </div>
            <div class="social-button whatsapp">
              <a href="https://api.whatsapp.com/send?&text=Klippy%20Website%20in%20React%20Js%20created%20by%20Sagar Mistry%20given%20link%20below http%3A%2F%2Fsbmshoutout.herokuapp.com" target="_blank"> 
              {/* <span class="icon icon-sharrow"></span> */}
              <i class="fa fa-whatsapp font30 text-white"></i>
                <span class="social-count"></span>
              </a>
            </div>
            </div>
    </div>
    </div>
    <Loader loading={this.state.loading} />
      </div>
    );
  }
}

export default Historydone;