import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Enterbankdetails extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row justify-content-center">
                        <div className="col-lg-6 bg-white rounded shadow-lg p-4">
                            <form className="w-100 d-flex justify-content-center flex-wrap">
                                <h5 className="w-100 mb-4 font-weight-bold">Enter Bank Details</h5>
                                <div class="form-group w-100">
                                    <input type="text" class="form-control" placeholder="Enter name" name="text1"/>
                                </div>
                                <div class="form-group w-100">
                                    <input type="number" class="form-control" placeholder="Enter account number" name="text1"/>
                                </div>
                                <div class="form-group w-100">
                                    <input type="number" class="form-control" placeholder="Enter IFSC Code" name="text1"/>
                                </div>
                                    <p className="text-center pl-5 pr-5 grey-color">This information will be securely saved as per our terms of service and privacy policy</p>
                                <button className="btn btn-primary pribg-color radius-full">SAVE DETAILS</button>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Enterbankdetails;