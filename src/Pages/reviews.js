import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Reviews extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  
  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-6 bg-white rounded shadow-lg p-4">
                                <h5 className="w-100 mb-3 font-weight-bold">Reviews</h5>
                                <div className="border-bottom pb-2 pt-3">
                                    <div className="d-flex align-items-center">
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                    </div>
                                    <p className="comments grey-color mt-1 mb-1">“There are many variations of passages of Lorem Ipsum available, but the majority”</p>
                                </div>
                                <div className="border-bottom pb-2 pt-3">
                                    <div className="d-flex align-items-center">
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                        <i className="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
                                    </div>
                                    <p className="comments grey-color mt-1 mb-1">“There are many variations of passages of Lorem Ipsum available, but the majority”</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Reviews;