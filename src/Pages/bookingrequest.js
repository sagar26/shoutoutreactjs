import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
import {Link} from 'react-router-dom';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import StarRatings from 'react-star-ratings';
import {retrieveItem} from '../component/retriveitem';
import Toast from '../utils/toast'
import Loader from '../utils/loader'
// import ReactPlayer from 'react-player';
import $ from 'jquery'

var selectvalue

class Bookingrequest extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
        name:'',
        notsame:false,
        modalVisible2:false
        // getid:this.props.match.params.id
      }
      this.getusertype()
      
  }
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
            currentdata:item,
            // otp: item.otp_verified,
            name: item.name,
            name1: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        const comingid =this.props.match.params.id
        // alert(comingid)
        this.fetchcele(comingid, this.state.token)
        this.notification(this.state.id,this.state.token);
    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
  //   // console.log('Welcome, '+this.state.name, 1000);
    // alert('Welcome, '+this.state.name)
    // alert(this.state.getid)
   
}
closenre=()=>{
    window.location.href = "/";
}
handlecomment=(e)=>{
    this.setState({ name1: e.target.value })
    setInterval(() => {
        if(this.state.name1 === this.state.name){
            this.setState({notsame:false})
            console.log('same')
        }else{
            console.log('not same')
            this.setState({notsame:true})
    
        }
    }, 1000);
  }
  handlecomment1=(e)=>{
    this.setState({ forname: e.target.value })
  }
  handlecomment2=(e)=>{
    this.setState({ description: e.target.value })
  }
fetchcele = async (id, token) => {
    // this.setState({ loading: true });
    var url = constant.SER_URL + '/celebrity-profile';
    console.log('url '+url)
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
      body: JSON.stringify({
        celebrity_id: id,
        platform: 'web',
      })
    })

      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        if (resmsg == "200") {
          // this.setState({ loading: true });
          this.setState({loadsk:false});
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          this.setState({
            celedata: response['data'],
            celeid: response['data']['id'],
            celename: response['data']['name'],
            celeprice: response['data']['price'],
            celecategory: response['data']['categories'],
            celepp: response['data']['profile_pic'],
            celetpp: response['data']['thumbnail_profile_pic'],
            celepv: response['data']['profile_video'],
            celedescription: response['data']['description'],
            celetags: response['data']['tags'],
            reviewccount: response['data']['average_rating'],
            celerating1: response['data']['celebrity_ratings'],
            celereview1: response['data']['celebrity_ratings'],
            shoutlist: response['data']['shoutouts_1'],
            // shoutlistname:response['data']['shoutouts_1'].app_user.customer_name,
            // shoutlistmedia:response['data']['shoutouts_1']['shoutout_media']['path'],
            // celerating:this.state.celerating1[this.state.celerating1.length-1].rating,
            // celereview:this.state.celereview1[this.state.celereview1.length-1].review,
            loadsk:true
          })
          console.log(JSON.stringify(response['data']));
          // // alert('200')
          // console.log(this.state.crating1);
          // alert(this.state.shoutlist.length);
          // console.log(JSON.stringify(this.state.shoutlist));
          // 
          $('select#sel1').on('change',function(){
              
            var x=this.value;
            // this.setState({
            //     person:x
            // })
            selectvalue=x
            if(x=== '1'){
            //   alert('other')
              $('#myselfsection').show()
            }else{
            //   alert('myself')
              $('#myselfsection').hide()
            }
          })
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error);
        this.setState({
          loading: false
        });
      });
    
  }
  todaydate=()=>{
    var today = new Date();
    var date;
    date=today.getDate() + "-"+ String(parseInt(today.getMonth()+1)).padStart(2, '0') +"-"+ today.getFullYear();
    return date
    // this.setState({todays:date})
    
    }
    
  submitrequest=(event)=>{
    event.preventDefault();
      var token =this.state.token
    // let videoF =selectvalue
    var videoF = $('select#sel1').val()
    if(videoF == '1'){
      this.datarender(token)
    // alert('datarender')
    }else{
        this.datarender1(token)
        // alert('datarender1')
    }
}

  datarender=async(token)=>{

    let id = this.state.celedata.id
    let videoF =$('select#sel1').val()
    let name =this.state.name1
    let forname = this.state.forname
    let description= this.state.description
    let publicc = '1'
    let todayss= this.todaydate()
    let body ={
        celebrity_id:id,
        video_for:videoF,
        name:name,
        description:description,
        shoutout_date:todayss,
        public:publicc,
        addressed_to:forname
    }
    // alert('token'+token)
    console.log('submit:-- ',body)
    this.setState({loading: true});
      var url=constant.SER_URL+'/submit-shoutout';
    await fetch(url,{
          method:'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              authorization:'Bearer'+token,
          },
          body: JSON.stringify(body)
      })

          .then((response) => response.json())
          .then((response) =>{
            var resmsg = response['code'];
            // alert(resmsg)
            if (resmsg == "200"){
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              this.setState({
                reqdata:response['data'],
              })
              console.log(JSON.stringify(response['data']));
              console.log(JSON.stringify(response['data']))
              this.staticpayment(token,response['data']['shoutoutId']['id'])
            }
            else if (resmsg == "400") {
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
            }
            else if (resmsg == "422") {
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              console.log(JSON.stringify(response['data']));
            }
          }).catch(error => {
            console.log('error: '+error, 1000);
          this.setState({
              loading: false
          });
      });
}


datarender1=async(token)=>{
    let id = this.state.celedata.id
    let videoF =$('select#sel1').val()
    let name =this.state.name1
    let forname = this.state.forname
    let description= this.state.description
    let publicc = '1'
    let todayss= this.todaydate()
    let body ={
        celebrity_id:id,
        video_for:videoF,
        name:name,
        description:description,
        shoutout_date:todayss,
        public:publicc,
        addressed_to:name
    }
    console.log('submit:-- ',body)
    this.setState({loading: true});
      var url=constant.SER_URL+'/submit-shoutout';
    await fetch(url,{
          method:'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              authorization:'Bearer'+token,
          },
          body: JSON.stringify(body)
      })
          .then((response) => response.json())
          .then((response) =>{
            var resmsg = response['code'];
            // alert(resmsg)
            if (resmsg == "200"){
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              this.setState({
                reqdata:response['data'],
              })
              console.log(JSON.stringify(response['data']));
            console.log(JSON.stringify(response['data']))
            this.staticpayment(token,response['data']['shoutoutId']['id'])
            }
            else if (resmsg == "400") {
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
            }
            else if (resmsg == "422") {
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              console.log(JSON.stringify(response['data']));
            }
          }).catch(error => {
            console.log('error: '+error, 1000);
          this.setState({
              loading: false
          });
      });
}

  staticpayment = async(token,id) => {
    // let id = this.state.celedata.id
    let todayss = this.todaydate()
    let body = {
      shoutout_id: id,
      payment_date: todayss,
    }

    // alert('tokenl '+token)
    console.log('Payment status: '+JSON.stringify(body))
    this.setState({ loading: true });
    var url = constant.SER_URL + '/shoutout-payment';
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
      body: JSON.stringify(body)
    })
      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        // alert(resmsg)
        if (resmsg == "200") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false,modalVisible2:true }); }, 1000);
          
          // this.setState({
          //   reqdata:response['data'],
          // })
          console.log(JSON.stringify(response['data']));
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 1000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 1000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error, 1000);
        this.setState({
          loading: false
        });
      });
  }
  notification = async (id,token) => {
    // this.setState({loading: true});
    var url = constant.SER_URL + '/notification-list';
    await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: {
            celebrity_id: id
        }
    })
    .then((response) => response.json())
    .then((response) => {
      console.log("notification", response)
            this.setState({
                notifydata: response['data']
            })
        })
        .catch(error => console.log(error)) //to catch the errors if any
  }
  daterender=(param)=>{
    var date = param;
    var datearray1 = date.split(" ");
    var datearray = datearray1[0].split("-");
    var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
    return this.time_ago(newdate)
  }
  time_ago=(time) =>{
  
    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) time = time.getTime();
        break;
      default:
        time = +new Date();
    }
    var time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;
  
    if (seconds == 0) {
      return 'Just now'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'from now';
      list_choice = 2;
    }
    var i = 0,
      format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
      }
    return time;
  }
  statuspara=(param)=>{
        switch (param) {
          case 'like':
            return 'fa-thumbs-up'
            break;
          case 'comment':
              return 'fa-comment'
              break;
          case 'shoutout-new-request':
            return 'fa-bell'
            break;
          case 'shoutout-request-accepted':
            return 'fa-check'
            break;
          case 'shoutout-request-rejected':
            return 'fa-ban'
            break;
            case 'celebrity-approved':
            return 'account-check'
            break;
            case 'celebrity-rejected':
            return 'account-remove'
            break;
          default:
              return 'fa-thumbs-up'
            break;
        }
      }
  render() {
    return (
        <div>
            {/* <Header /> */}
            <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class="nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold" href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  <div className="dropdown-menu dropdown-menu-right">
                  {this.state.selectid == this.state.id?null:<span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange1(k)} class="dropdown-item">View profile</span>}
                    <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                    {/* <span data-toggle="modal" data-target="#updateMobile" class="dropdown-item">Update mobile</span> */}
                    {/* <Link to="/enterbankdetails" class="dropdown-item">Enter bank details</Link> */}
                    {/* <span data-toggle="modal" data-target="#myModal" class="dropdown-item">
                      <p className="mb-1">Set booking price</p>
                      <b className="w-100 font14 orange-font">₹5000</b>
                    </span> */}
                    {
                      this.state.usertype == '2'?
                      <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                    }
                    
                    <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    {/* <Link to="/booking" class="dropdown-item">Booking</Link> */}
                  </div>
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <button class="btn pl-1 pr-1 position-relative"><i class="fa fa-bell-o text-white"></i>
              <div className="notify-point font40 position-absolute">.</div>
            </button>

            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row justify-content-center">
                        <div className="col-lg-6 bg-white rounded shadow-lg p-4">
                            {
                                this.state.loadsk?
                                <form className="w-100" onSubmit={this.submitrequest}>
                                <h5 className="w-100 mb-4">Request for</h5>
                               
                                <div class="form-group w-100">
                                    <select class="form-control bg-warning text-white border-0" id="sel1">
                                        <option value='1'>Other</option>
                                        <option value='2'>My Self</option>
                                    </select>
                                </div>
                                <div class="form-group w-100 ">
                                    <label className="font9 mb-0" for="usr">My name is</label>
                                    <div className='input-group'>
                                        <input type="text" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="usr" value={this.state.name1} placeholder={this.state.name} onChange={this.handlecomment} />
                                        <div class="input-group-append border-top-0 border-left-0 border-right-0 rounded-0" data-toggle={this.state.notsame?"tooltip":null} title={this.state.notsame?'are you sure? \nyou want to change name previous was '+this.state.name:null} style={{borderBottom:1,borderColor:"#ced4da",borderBottomStyle:"solid"}}>
                                        <i class={this.state.notsame?"fa fa-exclamation-triangle redf":null} aria-hidden="true" style={{fontSize: "1.5rem"}}></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group w-100" id='myselfsection'>
                                    <label className="font9 mb-0" for="dedicate">I dedicate this shoutout to</label>
                                    <input type="text" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="dedicate" onChange={this.handlecomment1}/>
                                </div> 
                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="desc">And my description is</label>
                                    <textarea type="text" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="desc"  onChange={this.handlecomment2}/>
                                </div>

                                {/* <h5 className="w-100 mb-4">Delivery Information</h5>

                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="email">My email </label>
                                    <input type="email" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="email" />
                                </div>  */}
                                
                                <div className="w-100 text-center">
                                <button type='submit' className="btn font-weight-bold pribg-color radius-full text-white text-uppercase">
                          REQUEST {this.state.celename?this.state.celename:'sagar mistry'}<b className="ml-3 font14">₹{this.state.celeprice?this.state.celeprice:'5000'}</b></button>
                                </div>
                            </form>:<Newsktn count={0} size={"100%"} sizeH={300} />
                            }
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <Loader loading={this.state.loading} />
            <Footer />

            <div style={{display:this.state.modalVisible2?" block":"none"}}>
          <div className="modal fade show modelcenter" id="myModal" style={{display:"block"}}>
          <div className="modal-dialog" style={{justifyContent:"center"}}>
          <div className="modal-content1">
                <div className="modal-body">
                {/* <div className="loader"></div> */}
                <br/>
                <div class="success-checkmark">
                    <div class="check-icon">
                        <span class="icon-line line-tip"></span>
                        <span class="icon-line line-long"></span>
                        <div class="icon-circle"></div>
                        <div class="icon-fix"></div>
                    </div>
                    </div>
                    <center>
                        <h3>Payment Successful!</h3>
                    </center><br/>
                    <center>
                    <button id="restart" className="btn font-weight-bold pribg-color radius-full text-white text-uppercase" onClick={this.closenre}>Close</button>
                    </center>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-backdrop fade show"></div>
        </div>
        </div>
    );
  }
}

export default Bookingrequest;