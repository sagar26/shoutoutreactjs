import React, { Component } from 'react';
//import Header from '../component/header';
import Footer from '../component/footer';
//import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import ReactPlayer from 'react-player';
// import Bookingrequest from './bookingrequest';
import {Link} from 'react-router-dom';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import StarRatings from 'react-star-ratings';
import {retrieveItem} from '../component/retriveitem';
import Toast from '../utils/toast'
import Loader from '../utils/loader'


class Booking extends Component {
  constructor(props) {
    super(props);
    this.state = {
      muted: true,
      paused: false,
      clicks: 0,
      show: true,
      toast:false,
      more:true,
      getid:this.props.match.params.id
    }
    this.getusertype();
    // this.newuser();
    // this.routeChange = this.routeChange.bind(this);
  }
  // getusertype = async () => {
  //   // retrieveItem
  //   var storedClicks;
  //   if (localStorage.getItem('clicks')) {
  //     storedClicks = parseInt(localStorage.getItem('clicks'));
  //   }
  //   let id = this.props.match.params.id
  //   let token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2NlbGVicml0eS1wYXR0ZXJuL3B1YmxpYy9hcGkvdjEvbG9naW4iLCJpYXQiOjE1NjUwNjg2NzYsImV4cCI6MTU5NjYwNDY3NiwibmJmIjoxNTY1MDY4Njc2LCJqdGkiOiJJQ1VzcWFBdmdWY1hCcmlOIiwic3ViIjo0LCJwcnYiOiI2MTM0MDBlZmIxYmZiNTBmOTZmOTBiM2VmZjNlYjFlNDlkZjRlODFkIn0.bJkjY4ClQ00_Q9vYVQulDqPR0-ZwiSlmTE_9nOjijRA"
  //   console.log(token, id);
  //   this.fetchcele(id, token)
  //   this.state = {
  //     clicks: storedClicks,
  //   };
  //   // retrieveItem
  //   // alert('Welcome, '+this.state.clicks);

  // }
  getusertype= async()=>{
    this.setState({ toast: false });
    // let userdata =localStorage.getItem('userData');
    // console.log("userdata:-"+userdata)
    //   retrieveItem
   await retrieveItem('userData').then((item)=>{
    
    setTimeout(() => {
      this.setState({ toast: true });
    }, 1000);
    
     console.log('retrieveItem:- '+item)
        this.setState({
            currentdata:item,
            // otp: item.otp_verified,
            name: item.name,
            email: item.email,
            updatedat: item.updated_at,
            createdat: item.created_at,
            id: item.id,
            usertype: item.user_type,
            price: item.price,
            token:item.token
        });
        // let id = this.props.match.params.id
        this.setState({
          selectid:this.state.getid
        })
        this.fetchcele(this.state.getid, this.state.token)
        this.fetchreview(this.state.getid,this.state.token);
        this.fetchfollow(this.state.getid, this.state.token)
        this.notification(this.state.id,this.state.token);
        
    }).catch((error)=>{
        console.log('Promise is rejected with erro',error)
    })
    setTimeout(() => {
      this.setState({ toast: false });
    }, 3000);
  //   //   retrieveItem
  //   // console.log('Welcome, '+this.state.name, Toasty);
    // alert('Welcome, '+this.state.name)
    // alert(this.state.getid)
   
}

  fetchcele = async (id, token) => {
    // this.setState({ loading: true });
    var url = constant.SER_URL + '/celebrity-profile';
    console.log('url '+url)
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
      body: JSON.stringify({
        celebrity_id: id,
        platform: 'web',
      })
    })

      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        if (resmsg == "200") {
          // this.setState({ loading: true });
          this.setState({loadsk:false});
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          this.setState({
            celedata: response['data'],
            celeid: response['data']['id'],
            celename: response['data']['name'],
            celeprice: response['data']['price'],
            celecategory: response['data']['categories'],
            celepp: response['data']['profile_pic'],
            celetpp: response['data']['thumbnail_profile_pic'],
            celepv: response['data']['profile_video'],
            celedescription: response['data']['description'],
            celetags: response['data']['tags'],
            reviewccount: response['data']['average_rating'],
            celerating1: response['data']['celebrity_ratings'],
            celereview1: response['data']['celebrity_ratings'],
            shoutlist: response['data']['shoutouts_1'],
            // shoutlistname:response['data']['shoutouts_1'].app_user.customer_name,
            // shoutlistmedia:response['data']['shoutouts_1']['shoutout_media']['path'],
            // celerating:this.state.celerating1[this.state.celerating1.length-1].rating,
            // celereview:this.state.celereview1[this.state.celereview1.length-1].review,
          })
          console.log(response['data']);
          // // alert('200')
          // console.log(this.state.crating1);
          // alert(this.state.shoutlist.length);
          // console.log(JSON.stringify(this.state.shoutlist), Toasty);
          // 
          const newData =  this.state.shoutlist.map(item=>{
            const itemData = `${item.id}`;
            return this.fetchlikes(itemData,this.state.token);
          })
          console.log('likes',newData)
          let geet, geet1,geet2
          if (this.state.celerating1 != '' || this.state.celerating1.length >-1) {
            geet = this.state.celerating1[this.state.celerating1.length - 1].rating
            // geet1 = this.state.celereview1[this.state.celereview1.length - 1].review
          }
          else {
            geet = ''
          }
          if (this.state.celereview1 != '' || this.state.celereview1.length >-1) {
            // geet = this.state.celerating1[this.state.celerating1.length - 1].rating
            geet1 = this.state.celereview1[this.state.celereview1.length - 1].review
          }
          else {
            geet1 = ''
          }
          geet2 = this.state.celerating1.length
          this.setState({
            crating: '2',
            crating1: geet,
            creview: geet1,
            reviewcount: geet2,
            countlist: this.state.shoutlist.length
          })
          if(this.state.countlist>=1){
            this.setState({loadsk:true});
          }else{
            this.setState({loadsk:false});
          }
          console.log(geet1);              
          
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error);
        this.setState({
          loading: false
        });
      });
    
  }
  fetchfollow=async(id,token)=>{
  
    // this.setState({loading: true});
      var url=constant.SER_URL+'/get-follows';
    await fetch(url,{
          method:'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              authorization:'Bearer'+token,
          }
      })
          .then((response) => response.json())
          .then((response) =>{
            var resmsg = response['code'];
            if (resmsg == "200"){
              // this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              this.setState({
                followdata:response['data'],
                follower:response['data']['followers'],
                following:response['data']['following'],
                followercount:response['data']['followers'].length,
                followingcount:response['data']['following'].length,
              })
              setTimeout(() => {
                this.setState({
                    loadsk:true
                })
            }, 1000);
              console.log(JSON.stringify(response['data']));
              const newData =  this.state.following.filter(item=>{
                const itemData = `${item.following_id}`;
                return itemData.indexOf(this.state.isdata.id) > -1;
              })
              this.setState({ getfollow:JSON.stringify(newData),countty:newData.length,getstatus:JSON.stringify(newData.status) })
            }
            else if (resmsg == "400") {
              this.setState({loading: true});
              setTimeout(() => { this.setState({loading: false}); },1000);
            console.log(JSON.stringify(response['message']));
            }
            else if (resmsg == "422") {
              this.setState({loading: true}); 
              setTimeout(() => { this.setState({loading: false}); }, 1000);
              
              console.log(JSON.stringify(response['data']));
            }
          }).catch(error => {
            console.log('error: '+error);
          this.setState({
              loading: false
          });
      });
  }
  routeChange(e) {
    // let path = "./bookingrequest";
    // this.props.history.push(path);
    let val = e.currentTarget.getAttribute('data-requestid')
    window.location.href = "/bookingrequest/"+val;
    console.log(val)
  }
  routeChange1 =(e)=>{
    // var op={
    //   response:ev.target.dataset.value
    // }
    let val = e.currentTarget.getAttribute('data-value')
    // let path = '/booking/';
    // this.props.history.push('/booking');
    window.location.href = "/booking/"+val;
    console.log(val)
  }
  IncrementItem = (item) => {
    this.setState({ clicks: this.state.clicks + 1, count: item })
    console.log('count', item)
  }

  starcount = (count) => {
    let rate = []
    for (let i = 1; i <= 5; i++) {
      rate.push(
        <div className="star" key={i}>
          {
            count >= i ?
              <i class="fa fa-star font12 mr-2 yellow-font" aria-hidden="true"></i>
              :
              <i class="fa fa-star-o font12 mr-2 yellow-font" aria-hidden="true"></i>
          }</div>
      );
    }
    return rate
  }
  
  relCelebrity = () => {
    let relatedCel = [];
    for (let i = 0; i <= 10; i++) {
      relatedCel.push(
        <div className='category-list-main pl-4 pr-4' key={i}>
          <div className='category-list radius-full'>
            <img src={require('../img/celebrity1.png')} alt='test' />
          </div> <h6 className='text-center mt-1 mb-0 font10 w-100'>Lisa Headen {i}</h6>
          <p className='text-center mt-0 grey-color font8 w-100'>Actor {i}</p>
        </div>
      );
    }
    return relatedCel;
  }

  videosMadeBy = () => {
    let latestVid = [];
    for (let i = 0; i < 4; i++) {
      latestVid.push(
        <div className="latestVid" key={i}>
          <div className={this.state.paused ? "ReactPlayerMain hiden" : "ReactPlayerMain"}>
            <div className="like">
              <h4 className="mb-0">0</h4>
              <button onClick={() => this.IncrementItem(i + 1)} className="btn likebutton bg-transparent"><i class="fa fa-thumbs-up font16 text-white"></i></button>
            </div>

            <Link to="/historydone" className="btn commentbutton bg-transparent"><i class="fa fa-commenting font16 text-white"></i></Link>

            <button onClick={() => this.setState({ paused: !this.state.paused })} className="btn playbutton bg-transparent">
              <i class={this.state.paused ? "fa fa-pause rubberBand animated font30 text-white" : "fa fa-play font30 text-white"}></i>
            </button>

            <ReactPlayer className="ReactPlayer" url={require('../img/klippyvideo.mp4')} loop playing={this.state.paused} />
            <div className="videoBottom">
              <div className="videoProPik radius-full">
                <img src={require('../img/celebrity1.png')} alt='test' />
              </div>
              <div className="forWhom">
                <h6 className="for-w font-weight-bold mb-0 text-white lineHight15">for Roshan</h6>
                <p className="date font8 mb-0 text-white lineHight15">15 Aug 2019</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return latestVid;
  }

  following = () => {
    let followingContant = [];
    for (let i=1; i<=5; i++){
      followingContant.push(
        <div className="w-100 pt-3 pb-1 border-bottom float-left">
          <div className="bookedCeleb1 radius-full float-left">
            <img src={require('../img/celebrity1.png')} alt='test' />
          </div>
          <div className="idmain2 float-left">
            <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
            <div className="w-50 font8 float-left grey-color">Actor</div>
            <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">UNFOLLOW</button>
          </div>
        </div>
      );
    }
    return followingContant;
  }

  followers = () => {
    let followersContant = [];
    for (let i=1; i<=4; i++){
      followersContant.push(
        <div className="w-100 pt-3 pb-1 border-bottom float-left">
          <div className="bookedCeleb1 radius-full float-left">
            <img src={require('../img/celebrity1.png')} alt='test' />
          </div>
          <div className="idmain2 float-left">
            <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
            <div className="w-50 font8 float-left grey-color">Actor</div>
            <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1">REMOVE</button>
          </div>
        </div>
      );
    }
    return followersContant;
  }
    
  reviewAll = () => {
    let reviewContant = [];
    for (let i=1; i<=6; i++){
      reviewContant.push(
        <div className="lightBlue pl-3 pr-3 pt-2 pb-2 border-bottom">
          <div className="w-100 d-flex align-items-center justify-content-between">
            <div>
              {this.starcount(this.state.crating1)}
            </div>
          </div>
          <p className="comments grey-color mt-1 mb-1">"{this.state.creview ? this.state.creview : "no review"}"</p>
        </div>
      );
    }
    return reviewContant;
  }
  fetchreview=async(id,token)=>{
    // this.setState({loading: true});
      var url=constant.SER_URL+'/get-all-reviews';
    await fetch(url,{
          method:'POST',
          headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
              authorization:'Bearer'+token,
          },
          body: JSON.stringify({
            celebrity_id:id,
            // platform: 'web',
          })
      })

          .then((response) => response.json())
          .then((response) =>{
            var resmsg = response['code'];
            if (resmsg == "200"){
              // this.setState({loading: true});
              // setTimeout(() => { this.setState({loading: false}); }, 1000);
              this.setState({
                reviewdata:response['data'],
                rrating:response['data']['rating'],
                rreview:response['data']['review']
              })
              console.log(JSON.stringify(response['data']));
            }
            else if (resmsg == "400") {
              // this.setState({loading: true});
              // setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
            }
            else if (resmsg == "422") {
              // this.setState({loading: true}); 
              // setTimeout(() => { this.setState({loading: false}); }, 1000);
              
              console.log(JSON.stringify(response['data']));
            }
          }).catch(error => {
            console.log('error: '+error);
          this.setState({
              loading: false
          });
      });

  }
  // likes = () => {
  //   let likesContant = [];
  //   for (let i = 1; i <= 6; i++) {
  //     likesContant.push(
  //       <div className="w-100 pt-3 pb-3 border-bottom d-flex flex-wrap">
  //         <div className="bookedCeleb1 radius-full">
  //           <img src={require('../img/celebrity1.png')} alt='test' />
  //         </div>
  //         <div className="d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
  //       </div>
  //     );
  //   }
  //   return likesContant;
  // }

  changeRating=( newRating)=> {
    return this.setState({
      rating: newRating
    });
  }
  
  moreless=()=>{
    this.setState({more:!this.state.more})
      }
      handlereview=(e)=>{
        this.setState({ textnumber: e.target.value })
    }
    addreviewsubmit=async(id,rating,review)=>{
      
      // alert('submit')
      // this.props.navigation.navigate('ReviewScreen')
      let token =this.state.token
      console.log(id+'.....'+rating+'....'+review+'....'+token)
      // this.setState({loading: true});
        var url=constant.SER_URL+'/celebrity-rating-review';
      await fetch(url,{
            method:'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                authorization:'Bearer'+token,
            },
            body: JSON.stringify({
              celebrity_id:id,
              rating:rating,
              review:review
            })
        })
  
            .then((response) => response.json())
            .then((response) =>{
              var resmsg = response['code'];
              if (resmsg == "200"){
                // this.setState({loading: true});
                // setTimeout(() => { this.setState({loading: false}); }, 1000);
                // this.setState({modalVisible:false,paused:false})
                // this.props.navigation.navigate('ReviewScreen',{isdata:this.state.celeid})
                window.location.reload();
              }
              else if (resmsg == "400") {
                // this.setState({loading: true});
                // setTimeout(() => { this.setState({loading: false}); }, 1000);
              console.log(JSON.stringify(response['message']));
              }
              else if (resmsg == "422") {
                // this.setState({loading: true}); 
                // setTimeout(() => { this.setState({loading: false}); }, 1000);
                
                console.log(JSON.stringify(response['data']));
              }
            }).catch(error => {
              console.log('error: '+error);
            this.setState({
                loading: false
            });
        });
  }
  submitremove=async(id)=>{
    let token =this.state.token
    this.setState({loading: true});
    var url=constant.SER_URL+'/follower-remove';
  await fetch(url,{
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: JSON.stringify({
          follower_id:id
        })
    })
        .then((response) => response.json())
        .then((response) =>{
          var resmsg = response['code'];
          if (resmsg == "200"){
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
            window.location.reload();
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true}); 
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            
            console.log(JSON.stringify(response['data']));
          }
        }).catch(error => {
          console.log('error: '+error);
        this.setState({
            loading: false
        });
    });
  }
  submitunfollow=async(id)=>{
    let token =this.state.token
    this.setState({loading: true});
    var url=constant.SER_URL+'/following-unfollow';
  await fetch(url,{
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: JSON.stringify({
          following_id:id
        })
    })
        .then((response) => response.json())
        .then((response) =>{
          var resmsg = response['code'];
          if (resmsg == "200"){
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            console.log(JSON.stringify(response['message']));
            window.location.reload();
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true}); 
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            
            console.log(JSON.stringify(response['data']));
          }
        }).catch(error => {
          console.log('error: '+error);
        this.setState({
            loading: false
        });
    });
  }
  follow=async(id,token)=>{
    // this.setState({loading: true});
    // alert(id+'-'+token);
    var url=constant.SER_URL+'/follow-celebrity';
    
  await fetch(url,{
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: JSON.stringify({
          celebrity_id:id,
        })
    })

        .then((response) => response.json())
        .then((response) =>{
          var resmsg = response['code'];
          if (resmsg == "200"){
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            this.setState({
                    changetext:false
                })
            console.log(response['message']);
            window.location.reload();
          }
          else if (resmsg == "400") {
            this.setState({loading: true});
            setTimeout(() => { this.setState({loading: false}); }, 1000);
          console.log(JSON.stringify(response['message']));
          }
          else if (resmsg == "422") {
            this.setState({loading: true}); 
            setTimeout(() => { this.setState({loading: false}); }, 1000);
            
            console.log(JSON.stringify(response['data']));
          }
        }).catch(error => {
          console.log('error: '+error);
        this.setState({
            loading: false
        });
    });
  }
  viewmsg=(param)=>{
    // this.toggleStatus(param)
    console.log('index>>',param)
    this.setState({
      setnum:param,
    })
  }
  toggleStatus(param){
    this.setState({
      paused:!this.state.paused
    })
  }

  historydone =(e)=>{
    let val = e.currentTarget.getAttribute('data-shoutceleid')
    let val1 =e.currentTarget.getAttribute('data-shoutid')
    window.location.href = "/historydone/"+val+'/'+val1;
    // this.props.history.push({
    //   pathname: '/historydone',
    //   data: val
    // })
    console.log(val+'/'+val1)
  }
  fetchlikes = async (id, token) => {
    // this.setState({ loading: true });
    var url = constant.SER_URL + '/shoutout-like-list';
    console.log('url '+url)
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        authorization: 'Bearer' + token,
      },
      body: JSON.stringify({
        shoutout_id: id,
      })
    })
  
      .then((response) => response.json())
      .then((response) => {
        var resmsg = response['code'];
        if (resmsg == "200") {
          // this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          this.setState({
            likesdata: response['data']['likes'],
          })
          console.log(response['data']['likes']);
        }
        else if (resmsg == "400") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['message']));
        }
        else if (resmsg == "422") {
          this.setState({ loading: true });
          setTimeout(() => { this.setState({ loading: false }); }, 2000);
          console.log(JSON.stringify(response['data']));
        }
      }).catch(error => {
        console.log('error: ' + error);
        this.setState({
          loading: false
        });
      });
  }
  notification = async (id,token) => {
    // this.setState({loading: true});
    var url = constant.SER_URL + '/notification-list';
    await fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            authorization:'Bearer'+token,
        },
        body: {
            celebrity_id: id
        }
    })
    .then((response) => response.json())
    .then((response) => {
      console.log("notification", response)
            this.setState({
                notifydata: response['data']
            })
        })
        .catch(error => console.log(error)) //to catch the errors if any
  }
  daterender=(param)=>{
    var date = param;
    var datearray1 = date.split(" ");
    var datearray = datearray1[0].split("-");
    var newdate = datearray[0] + '-' + datearray[1] + '-' + datearray[2];
    return this.time_ago(newdate)
  }
  time_ago=(time) =>{
  
    switch (typeof time) {
      case 'number':
        break;
      case 'string':
        time = +new Date(time);
        break;
      case 'object':
        if (time.constructor === Date) time = time.getTime();
        break;
      default:
        time = +new Date();
    }
    var time_formats = [
      [60, 'seconds', 1], // 60
      [120, '1 minute ago', '1 minute from now'], // 60*2
      [3600, 'minutes', 60], // 60*60, 60
      [7200, '1 hour ago', '1 hour from now'], // 60*60*2
      [86400, 'hours', 3600], // 60*60*24, 60*60
      [172800, 'Yesterday', 'Tomorrow'], // 60*60*24*2
      [604800, 'days', 86400], // 60*60*24*7, 60*60*24
      [1209600, 'Last week', 'Next week'], // 60*60*24*7*4*2
      [2419200, 'weeks', 604800], // 60*60*24*7*4, 60*60*24*7
      [4838400, 'Last month', 'Next month'], // 60*60*24*7*4*2
      [29030400, 'months', 2419200], // 60*60*24*7*4*12, 60*60*24*7*4
      [58060800, 'Last year', 'Next year'], // 60*60*24*7*4*12*2
      [2903040000, 'years', 29030400], // 60*60*24*7*4*12*100, 60*60*24*7*4*12
      [5806080000, 'Last century', 'Next century'], // 60*60*24*7*4*12*100*2
      [58060800000, 'centuries', 2903040000] // 60*60*24*7*4*12*100*20, 60*60*24*7*4*12*100
    ];
    var seconds = (+new Date() - time) / 1000,
      token = 'ago',
      list_choice = 1;
  
    if (seconds == 0) {
      return 'Just now'
    }
    if (seconds < 0) {
      seconds = Math.abs(seconds);
      token = 'from now';
      list_choice = 2;
    }
    var i = 0,
      format;
    while (format = time_formats[i++])
      if (seconds < format[0]) {
        if (typeof format[2] == 'string')
          return format[list_choice];
        else
          return Math.floor(seconds / format[2]) + ' ' + format[1] + ' ' + token;
      }
    return time;
  }
  statuspara=(param)=>{
        switch (param) {
          case 'like':
            return 'fa-thumbs-up'
            break;
          case 'comment':
              return 'fa-comment'
              break;
          case 'shoutout-new-request':
            return 'fa-bell'
            break;
          case 'shoutout-request-accepted':
            return 'fa-check'
            break;
          case 'shoutout-request-rejected':
            return 'fa-ban'
            break;
            case 'celebrity-approved':
            return 'account-check'
            break;
            case 'celebrity-rejected':
            return 'account-remove'
            break;
          default:
              return 'fa-thumbs-up'
            break;
        }
      }
  render() {

    let rating=1;

    return (

      <div>
        
        <div className="modal fade" id="myModal">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Add Review</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div className="pl-2 pb-3">
                   {/* <i class="fa fa-star-o font20 mr-2 yellow-font" aria-hidden="true"></i> */}
                    <StarRatings
                      rating={this.state.rating}
                      starRatedColor="#E9B500"
                      starHoverColor="#E9B500"
                      changeRating={this.changeRating}
                      numberOfStars={5}
                    />
                  </div>
                  <div className="form-group mb-0">
                    <textarea type="text" className="form-control border-0" placeholder="Add review here" id="usr" name="username"
                    onChange={this.handlereview} />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="submit" className="btn pribg-color text-white" onClick={()=>this.addreviewsubmit(this.state.selectid,this.state.rating,this.state.textnumber)}>Submit</button>
              </div>
            </div>
          </div>
        </div>

         {/* <Header/> */}
         <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name?this.state.name:"Guest"}/>
        <header>
        <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        {/* <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link> */}
        <Link to="/" className="font-weight-bold font18 text-white"><img src={require('../img/KLIPPY.png')} className='imglogo'/></Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          {
            this.state.id?
            <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul>
            :
          <ul className="navbar-nav font-weight-bold">
          <li className="nav-item">
            <Link to="/signup" className="nav-link loginText">Sign up</Link>
          </li>
          <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
          </li>    
        </ul>
          }
          <div className="after-login">

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2">
            <img src='https://i.imgur.com/oJKMjGR.png'/>
            </div>
                <li className="nav-item dropdown list-unstyled">
                  <span class={this.state.usertype != null || this.state.usertype != undefined?"nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold":"nav-link  pl-0 pr-0 text-white font-weight-bold"} href="#" id="navbardrop" data-toggle="dropdown">{this.state.name?this.state.name:"Guest"}</span>
                  {
                    this.state.usertype != null || this.state.usertype != undefined?
                    <div className="dropdown-menu dropdown-menu-right">
                    <span data-value={this.state.id?this.state.id:'0'} onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                      <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                      {
                        this.state.usertype == '2'?
                        <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>:null
                      }
                      
                      <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    </div>:null
                  }
                    
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.notifydata ? this.state.notifydata.length >=3?this.state.notifydata.length+'+':this.state.notifydata.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {
                                                this.state.notifydata  ?
                                                    this.state.notifydata.slice(0,5).map((item, k) =>
                                                        <div class="dropdown-item d-flex align-items-center mainnoti" key={k}>
                                                        <div class="mr-3">
                                                          <div class="icon-circle bg-primary">
                                                            <i class={"fa "+this.statuspara(item.page)+" text-white"} aria-hidden="true"></i>

                                                          </div>
                                                        </div>
                                                        <div>
                                                          <div class="small text-gray-500">{this.daterender(item.created_at)}</div>
                                                          <span>{item.message}</span>
                                                        </div>
                                                      </div>
                                                    ) : <p><center>no notification</center></p>
                                            }
                                            {
                                              this.state.notifydata ?
                                                this.state.notifydata.length > 5 ?
                                                  <center>
                                                    {/* <button className="dropdown-item text-center small text-gray-500"> */}
                                                      <Link
                                                        to="/mynotification" className="dropdown-item text-center small text-gray-500">SEE MORE</Link>
                                                        {/* </button> */}
                                                  </center>
                                                   : null : null
                                             }
                                        </div>
                                    </div>

                                </div>


            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav> 
      </header>
        <div className="container-fluid pb-5">
        
          <div className="row">
            <div class="container-fluid pribg-color blueContainer"></div>
            <div className="container">
              <div className="row">
{
  this.state.loadsk?
  <div className="video-main">
                <div className="ReactPlayerMain-shadow mb-4">
                  <div className="ReactPlayerMain">
                    {/* <button className="btn btn-follow likePading"><i class="fa fa-heart font12"></i></button> */}
                    <button className="btn mutebutton bg-transparent" onClick={() => this.setState({ muted: !this.state.muted })}><i class={this.state.muted ? "fa fa-volume-off font22 text-white" : "fa fa-volume-up font22 text-white"}></i></button>
                    {
                      this.state.celepv!=null?
                      <ReactPlayer className="ReactPlayer-profile preview" url={this.state.celepv ? this.state.celepv: require('../img/klippyvideo.mp4')} playing loop muted={this.state.muted} />
                       :
                       <div className="ReactPlayer-profile preview"><img data-src={require('../img/novideo.png')} alt='no video found' className='lazy-img novideo' src={require('../img/novideo.png')}/></div>
                    }
                    
                  </div>
                  <div className="profile-details-main p-3">
                    <div className="d-flex justify-content-between flex-wrap align-items-baseline">
                      <p className="about font-weight-bold">ABOUT</p>
                      {
                        this.state.selectid == this.state.id?null
                      :
                      this.state.countty == "0"?<button type="button" className="btn btn-sm btn-outline-primary font-weight-bold" onClick={()=>this.follow(this.state.selectid,this.state.token)}>FOLLOW</button>:<button type="button" className="btn btn-sm btn-outline-primary font-weight-bold" onClick={()=>this.submitunfollow(this.state.selectid,this.state.token)}>Following</button>
                      }
                      
                    </div>
                    <p className="border-bottom pb-3 grey-color moreless">
                        {this.state.celedescription ? this.state.celedescription : "no description"}
                    </p>
                    
                    <div className="grey-color d-flex justify-content-between flex-wrap align-items-baseline mb-2">
                      <b className="font8">FOLLOWING</b><b className="font8">{this.state.followingcount?this.state.followingcount:'0'}</b>
                    </div>
                    <div className="grey-color d-flex justify-content-between flex-wrap align-items-baseline mb-2">
                      <b className="font8">FOLLOWERS</b><b className="font8">{this.state.followercount?this.state.followercount:'0'}</b>
                    </div>
                  </div>
                </div>
                {
                  this.state.selectid == this.state.id?null
                  :<div className="lightBlue w-100 p-3" style={{ clear: "both", borderRadius: "10px" }}>
                  <div className="w-100 d-flex align-items-center justify-content-between">
                    <div>
                      {this.state.creview?
                        this.starcount(this.state.crating1):<p className="comments grey-color mt-3">No Review</p>}
                    </div>
                    <button data-toggle="modal" data-target="#myModal" data-backdrop='static' data-keyboard='false' className="btn btn-sm btn-primary pribg-color font-weight-bold">ADD REVIEW</button>
                  </div>
                  {
                    this.state.creview?
                    <p className="comments grey-color mt-3">"{this.state.creview ? this.state.creview :null}"</p>:null
                  }
                  
                </div>
                }
                </div>:<div className="video-main"><Newsktn count={0} size={"100%"} sizeH={400} /></div>
}
                

                <div className="booking-cel-right pl-5 pt-4">
                <div className="w-100 booking-cel-name">
                  <div className="d-flex justify-content-between flex-wrap">
                  <h3 className="w-100 font-weight-bold blackWhite text-capitalize">{this.state.celename ? this.state.celename : "Celebrity Name"}</h3>
                  {
                    this.state.celecategory?
                    <div className="d-flex true flex-wrap">
                        {
                          this.state.celecategory?
                            this.state.celecategory.map((items,i)=><p className="w-auto blackWhite text-uppercase prl mb-0" key={i}>{items.name}{items[items.length-1] == items.name?null:' '}</p>)
                            :<p className="w-auto blackWhite text-uppercase mb-0">uncategorized</p>
                        }
                    </div>:
                    <div className="d-flex false flex-wrap">
                        <p className="w-auto blackWhite text-uppercase mb-0">uncategorized</p>
                    </div>
                }
                  </div>
                  <button className="btn btn-light font12" onClick={this.routeChange} data-requestid={this.state.celeid}>REQUEST <b className="ml-3 font14">₹{this.state.celeprice?this.state.celeprice:'5000'}</b></button>
                </div>
              
                  {/* <div className="w-100 mt-3">
                    <button onClick={this.routeChange} className="btn-booking btn font-weight-bold blackbg rounded-0 text-white">
                      REQUEST SACHIN TENDULKER<b className="ml-3 font14">₹100</b></button>
                    <p className="font-italic mt-1 pl-3">Usually responds in 1 hour</p>
                  </div> */}

                  <div className="mt-2">
                    <ul class="nav nav-tabs mb-4 border-0" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#shoutouts">Shoutouts</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#following">Following</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#followers">Followers</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#reviews">Reviews</a>
                      </li>
                      {/* <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#likes">Likes</a>
                      </li> */}
                    </ul>

                    <div class="tab-content">
                      <div id="shoutouts" class="container tab-pane active">
                      {/* <h3 className={this.state.shoutlist?'d-none':'d-flex'}>No Klippy</h3> */}
                        <div className="latest-right-vid">
                          {/* {this.videosMadeBy()} */}
                          
                          {
                            this.state.loadsk?
                            this.state.countlist>=1?
                            this.state.shoutlist.map((item,k)=>
                            <div className="latestVid" key={k}>
                              {/* {this.fetchlikes(item.id,this.state.token)} */}
                              <div className={this.state.paused ? "ReactPlayerMain hiden" : "ReactPlayerMain"}>
                                <div className="like" >
                                  <h4 className="mb-0">{this.state.likesdata?this.state.likesdata.length:'0'}</h4>
                                  <button  className="btn likebutton bg-transparent" onClick={this.historydone.bind(item)} data-shoutid={item.id} data-shoutceleid={this.state.getid}><i class="fa fa-thumbs-up font16 text-white"></i></button>
                                </div>

                                <button onClick={this.historydone.bind(item)} data-shoutid={item.id} data-shoutceleid={this.state.getid} className="btn commentbutton bg-transparent cursorpt"><i class="fa fa-commenting font16 text-white"></i></button>

                                <button onClick={() =>this.viewmsg(k)} className="btn playbutton bg-transparent">
                                  <i class={this.state.setnum == (k)  ? "fa fa-pause rubberBand animated font30 text-white" : "fa fa-play font30 text-white"}></i>
                                </button>

                                {/* <ReactPlayer className="ReactPlayer" url={item.path?{uri:item.path}:require('../img/klippyvideo.mp4')} loop playing={this.state.paused} /> */}
                                  {
                                    item.path ?
                                      <ReactPlayer className="ReactPlayer preview" url={item.path ? { uri: item.path } : require('../img/klippyvideo.mp4')} loop playing={this.state.setnum == (k) ?true:false} /> :
                                      <div className="ReactPlayer preview"><img data-src={require('../img/novideo.png')} alt='no video found' className='lazy-img novideo' src={require('../img/novideo.png')} /></div>
                                  }
                                <div className="videoBottom">
                                  <div className="videoProPik radius-full">
                                    <img src={item.path?{uri:item.path}:require('../img/celebrity1.png')} alt='test' />
                                  </div>
                                  <div className="forWhom">
                                    <h6 className="for-w font-weight-bold mb-0 text-white lineHight15">{item.app_user.customer_name}</h6>
                                    {/* <p className="date font8 mb-0 text-white lineHight15" onClick={this.routeChange.bind(item)}>
                                    <Link to="/historydone" className='text-white font8'>Details</Link>
                                    </p> */}
                                    <p className="date font8 mb-0 text-white lineHight15 cursorpt detcur" data-shoutid={item.id} data-shoutceleid={this.state.getid} onClick={this.historydone.bind(item)}>Details </p>
                                    <p className="date font8 mb-0 text-white lineHight15">{item.date?item.date:null}</p>
                                  </div>
                                </div>
                              </div>
                            </div>)
                            :<h3>No Klippy</h3>
                            :<div class="row">
                            <Newsktn count={1} size={"35%"} sizeH={400} />
                            </div>
                          }
                        </div>
                      </div>

                      <div id="following" class="container tab-pane fade">
                        {/* {this.following()} */}
                        {
                          this.state.followingcount?null:<h3>No Following</h3>
                        }
                        {/* <h3 className={this.state.following?'d-none':'d-flex'}>No Following</h3> */}
                        {
                          this.state.following || this.state.followingcount?
                          this.state.following.map((item, k) =>
                          <div className="w-100 pt-3 pb-1 border-bottom float-left" key={k}>
                            <div className="bookedCeleb1 radius-full float-left">
                              <img src={require('../img/celebrity1.png')} alt='test' />
                            </div>
                            <div className="idmain2 float-left">
                              <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">{item.app_user_1.name?item.app_user_1.name:'sagar'}</b></div>
                              <div className="w-50 font8 float-left grey-color">Actor</div>
                              <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1" onClick={()=>this.submitunfollow(item.following_id)}>UNFOLLOW</button>
                            </div>
                          </div>
                          )
                          :null
                        }
                      </div>

                      <div id="followers" class="container tab-pane fade">
                        {/* {this.followers()} */}
                        <h3 className={this.state.follower?'d-none':'d-flex'}>No Followers</h3>
                        {
                          this.state.follower || this.state.followercount?
                          this.state.follower.map((item, k) =>
                          <div className="w-100 pt-3 pb-1 border-bottom float-left" key={k}>
                            <div className="bookedCeleb1 radius-full float-left">
                              <img src={require('../img/celebrity1.png')} alt='test' />
                            </div>
                            <div className="idmain2 float-left">
                              <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">{item.app_user_1.name?item.app_user_1.name:'sagar'}</b></div>
                              <div className="w-50 font8 float-left grey-color">Actor</div>
                              <button className="btn btn-sm rupees font8 font-weight-bold float-right prif-color pt-1 pb-1" onClick={()=>this.submitremove(item.follower_id)}>REMOVE</button>
                            </div>
                          </div>
                          )
                          :null
                        }
                      </div>

                      <div id="reviews" class="container tab-pane fade">
                        {/* {
                          !this.state.reviewdata?<h3>No Review</h3>:null
                        } */}
                        <h3 className={this.state.reviewdata?'d-none':'d-flex'}>No Review</h3>
                        {/* {this.reviewAll()} */}
                        
                        {
                          this.state.reviewdata ?
                          this.state.reviewdata.map((item, k) =>
                          <div className="lightBlue pl-3 pr-3 pt-2 pb-2 border-bottom">
                            
                          <div className="w-100 d-flex align-items-center justify-content-between">
                            <div>
                            {this.starcount(item.rating)}
                            </div>
                          </div>
                          <p className="comments grey-color mt-1 mb-1">"{item.review}"</p>
                        </div>)
                        :null
                        }
                        
                      </div>

                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="container-fluid body-color">
        <div className="container pt-5 pb-5 border-bottom">
          <div className="row">
            <div className="col-lg-12"><h5 className="prif-color font-weight-bold mb-4">Related</h5></div>
            <OwlCarousel
            className="owl-theme"
            loop={false}
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.relCelebrity()}
        </OwlCarousel>
          </div>
        </div>
        </div> */}
    <Footer/>
    <Loader loading={this.state.loading} />
      </div>
    );
  }
}

export default Booking;