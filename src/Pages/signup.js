import React, { Component} from 'react';
import '../custom/login.css'

export class Signup extends Component {
    constructor(props){
        super(props);
        this.routeChange = this.routeChange.bind(this);
        this.list();
        this.state={
            shown:false,
            dataSource:[]
          };
          this.areCelebrity=this.areCelebrity.bind(this);

    }  

    areCelebrity = () =>{
        const{shown} = this.state;
        this.setState({shown : !shown })
    }

    routeChange(){
        let path="/signin";
        this.props.history.push(path);
    }



    list = async () => {
        // console.log('testing')
        var url = 'http://3.16.209.1/celebrity_pattern/public/api/v1/category';

        await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })

            .then(response => response.json())
            .then((responseJson) => {
                console.log('comin' + JSON.stringify(responseJson))
                this.setState({
                    dataSource: responseJson['data'],
                    loading:false
                })
                // alert(this.state.dataSource.length);
                console.log(responseJson['data']['name']);

            })
            .catch(error => console.log(error)) //to catch the errors if any
    }



    myLoop = () => {
        let customCard = [];
        for (let i = 0; i < this.state.dataSource.length; i++) {
            customCard.push(
                <option value={"apidata"}>{this.state.dataSource[i].name}</option>
            )
        }
        return customCard;
    }




  render() {
    return (
    <div className="container-fluid h-100">
        <div className="row h-100">
            <div className="signup-left pribg-color h-100 login-left-bg">
                
            </div>
            <div className="signup-right h-100 overflow-auto">
                    <div className="celebrity-block w-100">
                        <div className="pl-4 pr-4 mb-2">
                            <h5 className="prif-color font-weight-bold w-100 pb-4 font16">Sign up!</h5>
                            <form action="/action_page.php" className='w-100'>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="name" placeholder="Name" name="name"></input>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="email" placeholder="Email Id" name="email"></input>
                                </div>
                                <div className="form-group">
                                    <input type="number" className="form-control" id="m-number" placeholder="Mobile Number" name="mn"></input>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" id="pwd" placeholder="password" name="pswd"></input>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="dob" placeholder="DOB" name="dobt"></input>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="gen" placeholder="Gender" name="gend"></input>
                                </div>
                                <div className="form-group text-right d-flex justify-content-between align-items-center">
                                    <div className="font10 font-weight-bold grey-color pt-0 pb-0 font10">
                                    Are you a celebrity?
                                    </div>
                                    <div>
                                        <label className="switch mb-0">
                                            <input onClick={this.areCelebrity} type="checkbox" className="success" />
                                            <span className="slider round"></span>
                                        </label>
                                    </div>
                                </div>

                                {this.state.shown?
                                    <div id="celebForm">
                                        <div className="form-group">
                                            <select name="catgory"  class="custom-select " multiple data-live-search="true" size="3" id="a1" onChange="getSelectedValue">
                                                {
                                                    this.myLoop()
                                                }
                                            </select>
                                        </div>


                                    <div className="form-group">
                                        <select name="find" class="custom-select">
                                            <option selected>Where can we find you?</option>
                                            <option value="volvo">Volvo</option>
                                            <option value="fiat">Fiat</option>
                                            <option value="audi">Audi</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control" id="handle" placeholder="Your handle" name="yhandle"></input>
                                    </div>
                                    <div className="form-group">
                                        <input type="email" className="form-control" id="fow" placeholder="How many followers do you have?" name="fowr"></input>
                                    </div>
                                    </div>:null}

                                <div className="form-group d-flex justify-content-center pt-4">
                                    <button type="button" className="btn btn-info btn-lg pl-4 pr-4 d-flex align-items-center font10 font-weight-bold radius-full pribg-color">
                                        <span className="pull-left">SIGN UP</span>
                                        <i className="fa fa-long-arrow-right pull-right"></i>
                                    </button>
                                </div>
                                <div className="form-group d-flex justify-content-center">
                                    <button onClick={this.routeChange} type="button" className="btn pl-4 pr-4 d-flex align-items-center font-weight-bold grey-color font8">
                                        <u className="pull-left">Have an account? Log in</u>
                                    </button>
                                </div>

                                

                            </form>
                        </div>

                    </div>
            </div>
        </div>
    </div>
    );
  }
}
// class Box extends Component{
//     render(){
//         return(
//             <div id="celebForm">
//                                 <div className="form-group">
//                                     <select name="catgory" class="custom-select">
//                                         <option selected>Category</option>
//                                         <option value="volvo">Volvo</option>
//                                         <option value="fiat">Fiat</option>
//                                         <option value="audi">Audi</option>
//                                     </select>
//                                 </div>
//                                 <div className="form-group">
//                                     <select name="find" class="custom-select">
//                                         <option selected>Where can we find you?</option>
//                                         <option value="volvo">Volvo</option>
//                                         <option value="fiat">Fiat</option>
//                                         <option value="audi">Audi</option>
//                                     </select>
//                                 </div>
//                                 <div className="form-group">
//                                     <input type="email" className="form-control" id="handle" placeholder="Your handle" name="yhandle"></input>
//                                 </div>
//                                 <div className="form-group">
//                                     <input type="email" className="form-control" id="fow" placeholder="How many followers do you have?" name="fowr"></input>
//                                 </div>
//                                 </div>
//         )
//     }
// } 

export default Signup;
