import React, { Component } from 'react';
import {
  Route
} from 'react-router-dom';
import './App.css';
import '../src/css/bootstrap.min.css';
import "./custom/style.css";
import '../src/css/animate.css';
import '../src/owlcarouse/owl.carousel.min.css';
// import component
// import pages
import Home from './Pages/home';
import Signin from './Pages/signin';
import Signout from './Pages/signout';
import Signup from './Pages/signup';
import Forgot from './Pages/forgot';
import Booking from './Pages/booking';
import Bookinghistory from './Pages/bookinghistory';
import Historydone from './Pages/historydone';
import Updateprofile from './Pages/updateprofile';
import Bookingrequest from './Pages/bookingrequest';
// import Enterbankdetails from './Pages/enterbankdetails';
import Mytrasaction from './Pages/mytransaction';
// import Follow from './Pages/follow';
import Aboutus from './Pages/aboutus';
import Termsandcondition from './Pages/termsandcondition';
import Privacypolicy from './Pages/privacypolicy';
import Privacypolicyapp from './Pages/privacypolicyapp';
import Faq from './Pages/faq';
import Contactus from './Pages/contactus';
import Reviews from './Pages/reviews';
import Aboutusapp from './Pages/aboutusapp';
import Termsandconditionapp from './Pages/termsandconditionapp';
import MyNotification from './Pages/seemore'

// import pages
class App extends Component {
  
  render() {
    return (
      <div className="App">
        <div className="theme-showcase" role="main">
          {/* <Route exact path="/" component={Signin} /> */}
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/signin" component={Signin} />
          <Route exact path="/signout" component={Signout} />
          <Route exact path="/signup" component={Signup} />
          <Route exact path="/forgot" component={Forgot} />
          <Route exact path={"/booking/:id"} component={Booking} />
          <Route exact path="/bookinghistory" component={Bookinghistory} />
          <Route exact path="/historydone/:id/:ids" component={Historydone} />
          <Route exact path="/updateprofile" component={Updateprofile} />
          <Route exact path="/bookingrequest/:id" component={Bookingrequest} />
          {/* <Route exact path="/enterbankdetails" component={Enterbankdetails}/> */}
          <Route exact path="/mytrasaction" component={Mytrasaction} />
          {/* <Route exact path="/follow" component={Follow}/> */}
          <Route exact path="/aboutus" component={Aboutus} />
          <Route exact path="/aboutusapp" component={Aboutusapp} />
          <Route exact path="/termsandcondition" component={Termsandcondition} />
          <Route exact path="/termsandconditionapp" component={Termsandconditionapp} />
          <Route exact path="/Privacypolicy" component={Privacypolicy} />
          <Route exact path="/Privacypolicyapp" component={Privacypolicyapp} />
          <Route exact path="/faq" component={Faq} />
          <Route exact path="/contactus" component={Contactus} />
          <Route exact path="/reviews" component={Reviews} />
          <Route exact path="/mynotification" component={MyNotification} />
        </div>
      </div>
    );
  }
}

export default App;
