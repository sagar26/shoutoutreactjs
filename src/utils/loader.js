import React from 'react';
const Loader = props => {
    const {
        loading,
        ...attributes
        } = props;

    return (
        <div style={{display:loading?" block":"none"}}>
          <div className="modal fade show" id="myModal" style={{display:"block"}}>
          <div className="modal-dialog modal-dialog-centered" style={{justifyContent:"center"}}>
          <div className="modal-content1">
                <div className="modal-body">
                <div className="loader"></div>
                </div>
                
              </div>
            </div>
          </div>
          <div class="modal-backdrop fade show"></div>
        </div>
    )
}
export default Loader;