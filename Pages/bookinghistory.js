import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Bookinghistory extends Component {
  constructor(props){
    super();
    this.routeChange = this.routeChange.bind(this);
  }
  routeChange(){
    let path = "/historydone";
    this.props.history.push(path);
  }

  render() {
    return (
      <div className="body-color">
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title redf">Pending</h4>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                There is no shoutout.
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        
        <Header/>
        <div className="container-fluid pribg-color blueContainer"></div>
        <div className="container pb-5">
          <div className="row">
            <div className="container">
              <div className="col-lg-12 pl-0"><h5 className="font-weight-bold mb-4 text-white">Booking History</h5></div>
              <div className="row">
                <div className="col-lg-4 col-md-4 p-2" data-toggle="modal" data-target="#myModal">
                  <div className="card p-3">
                    <div className="w-100">
                      <div className="bookedCeleb radius-full float-left">
                        <img src={require('../img/celebrity1.png')} alt='test' />
                      </div>
                      <div className="idmain float-right">
                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2 redf">PENDING</b><i className="fa fa-chevron-right grey-color"></i></div>
                        <div className="w-100 font8 float-left font-weight-bold">ID:BOK15</div>
                      </div>
                    </div>
                    <b className="w-100 font10 mt-2">Akshay Kumar</b>
                    <i className="w-100 font9"><i className="grey-color mr-1">for</i><b>Roshan</b></i>
                    <div className="grey-color font8 mt-3"><i class="fa fa-calendar mr-2"></i>20 Jul 2018</div>
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 p-2" onClick={this.routeChange}>
                  <div className="card p-3">
                    <div className="w-100">
                      <div className="bookedCeleb radius-full float-left">
                        <img src={require('../img/celebrity1.png')} alt='test' />
                      </div>
                      <div className="idmain float-right">
                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2 prif-color">DONE</b><i className="fa fa-chevron-right grey-color"></i></div>
                        <div className="w-100 font8 float-left font-weight-bold">ID:BOK15</div>
                      </div>
                    </div>
                    <b className="w-100 font10 mt-2">Akshay Kumar</b>
                    <i className="w-100 font9"><i className="grey-color mr-1">for</i><b>Roshan</b></i>
                    <div className="grey-color font8 mt-3"><i class="fa fa-calendar mr-2"></i>20 Jul 2018</div>
                  </div>
                </div>
                <div className="col-lg-4 col-md-4 p-2" onClick={this.routeChange}>
                  <div className="card p-3">
                    <div className="w-100">
                      <div className="bookedCeleb radius-full float-left">
                        <img src={require('../img/celebrity1.png')} alt='test' />
                      </div>
                      <div className="idmain float-right">
                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2 prif-color">DONE</b><i className="fa fa-chevron-right grey-color"></i></div>
                        <div className="w-100 font8 float-left font-weight-bold">ID:BOK15</div>
                      </div>
                    </div>
                    <b className="w-100 font10 mt-2">Akshay Kumar</b>
                    <i className="w-100 font9"><i className="grey-color mr-1">for</i><b>Roshan</b></i>
                    <div className="grey-color font8 mt-3"><i class="fa fa-calendar mr-2"></i>20 Jul 2018</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    <Footer/>
      </div>
    );
  }
}

export default Bookinghistory;