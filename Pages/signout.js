import React, { Component,AsyncStorage} from 'react';
import '../custom/login.css';
import Loader from '../utils/loader'

export default class Signout extends Component {
      constructor(props){
          super(props);
      this.load()
     };

     load=()=>{
        localStorage.clear();
        setTimeout(() => {
            this.props.history.push('/home');
            // alert('home')
        }, 2000);
      }
  render() {
    return (
      <Loader loading={true} />
    );
  }
}

