import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
// import ReactPlayer from 'react-player';

class Updateprofile extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row justify-content-center">
                    <div className="col-lg-10">
                        <div className="row">
                            <div className="updatepik mb-4 shadow mr-2">
                                <img src={require('../img/pro-default.png')} alt='test' />
                                <div className="camera">
                                    <img src={require('../img/camera.png')} alt='test' />
                                </div>
                            </div>
                            <div className="col-md-9">
                                <h3 className="blackWhite"><b>Sachin Tendulker</b></h3>
                                <p className="blackWhite">CRICKETER</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-lg-10 bg-white rounded shadow-lg p-4">
                        <div className="row">
                            <form className="w-100 d-flex justify-content-center flex-wrap">

                                <div className="col-md-12 mb-2 d-flex align-items-center">
                                    <h6 className="w-100 font-weight-bold mb-0">Update profile</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="First Name" name="text1" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="DOB" name="text1" />
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <div class="form-group w-100">
                                        <textarea type="text" class="form-control" placeholder="Description" name="text1" />
                                    </div>
                                </div>


                                <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Update mobile</h6>
                                </div>
                                <div className="col-md-10">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Enter mobile number" name="text1" />
                                    </div>
                                </div>
                                <div className="col-md-2">
                                    <div class="form-group w-100">
                                        <button className="btn btn-primary pull-right">SEND OTP</button>
                                    </div>
                                </div>


                                <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Enter bank details</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Enter bank Ac/ number" name="text1" />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Enter IFSC code" name="text1" />
                                    </div>
                                </div>


                                <div className="col-md-12 mb-2 d-flex align-items-center mt-2">
                                    <h6 className="w-100 font-weight-bold mb-0">Set booking price</h6>
                                    <button className="btn"><i class="fa fa-edit"></i></button>
                                </div>
                                <div className="col-md-12">
                                    <div class="form-group w-100">
                                        <input type="text" class="form-control" placeholder="Your booking price" name="text1" />
                                    </div>
                                </div>

                                <div class="form-group w-100 mt-3 text-center">
                                    <button class="btn btn-lg btn-primary"> SAVE DETAILS</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Updateprofile;