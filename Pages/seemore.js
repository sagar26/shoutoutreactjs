import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Seemore extends Component {
    constructor(props){
        super();
        this.state={
            muted: true,
            paused:false,
            clicks: 0,
            show:true,
            dataSource:[],
            loading:true
        }
        this.newnotification();
    }

    newnotification = async()=> {
        this.setState({loading: true});
        var url = 'http://192.168.2.42/celebrity-pattern/public/api/v1/notification-list';
        await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2NlbGVicml0eS1wYXR0ZXJuL3B1YmxpYy9hcGkvdjEvbG9naW4iLCJpYXQiOjE1NjUwNjg2NzYsImV4cCI6MTU5NjYwNDY3NiwibmJmIjoxNTY1MDY4Njc2LCJqdGkiOiJJQ1VzcWFBdmdWY1hCcmlOIiwic3ViIjo0LCJwcnYiOiI2MTM0MDBlZmIxYmZiNTBmOTZmOTBiM2VmZjNlYjFlNDlkZjRlODFkIn0.bJkjY4ClQ00_Q9vYVQulDqPR0-ZwiSlmTE_9nOjijRA'
            },
            body:{
                celebrity_id:5
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                // console.log('comin1234' + JSON.stringify(responseJson));
                this.setState({
                    dataSource: responseJson['data']
                })
            })
            .catch(error => console.log(error)) //to catch the errors if any
    }
    notificationLoop=()=> {
        let notifyArray = [];

        for (let i = 0; i < this.state.dataSource.length; i++) {
            notifyArray.push(
                <div className="notification-card">
                    <div className="notify-header">
                        <div className="notification-title">
                            <h4>{this.state.dataSource[i].title}</h4>
                        </div>
                        <div className="date-time">
                            <p>{this.state.dataSource[i].created_at}</p>
                        </div>
                    </div>
                    <p>{this.state.dataSource[i].message}</p>


                </div>
            )
        }
        return notifyArray;
    }
    render() {

        return (
            <div>
                <Header/>
                <div className="container-fluid pribg-color blueContainer"></div>
                <div className="container pb-5 border-bottom">
                    <div className="row">
                        <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4">
                            <h5 className="prif-color font-weight-bold mb-3">ALL NOTIFICATIONS</h5>
                            <div className="notification-container">
                                <div className="shadow1 mb-3 ">
                                    {/*{this.notificationLoop()}*/}
                                    {
                                        this.state.dataSource?

                                            this.state.dataSource.map((item, k) =>
                                                <div className="notification-card" key={k}>
                                                    <div className="notify-header">
                                                        {/*<div className="notification-title">*/}

                                                        {/*<h4>{item.title}</h4>*/}

                                                        {/*</div>*/}
                                                        <div className="date-time">
                                                            <p>{item.created_at}</p>
                                                        </div>
                                                    </div>
                                                    <p>{item.message}</p>
                                                </div>
                                            ) : <p>
                                                <center>no notification</center>
                                            </p>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Seemore;