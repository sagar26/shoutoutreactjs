import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Mytrasaction extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  
  loop = () => {
      let customCard = [];
      for (let i = 0; i < 10; i++){
          customCard.push(
              <div key={i} className="col-lg-2 col-md-3 p-2">
                  <div className="customCard shadow p-3">
                      <div className="w-100 pull-left">
                          <div className="bookedCeleb2 radius-full pull-right">
                              <img src={require('../img/celebrity1.png')} alt='test' />
                          </div>
                      </div>

                      <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                      <div className="rupees font14 font-weight-bold pull-right prif-color">+₹500</div>
                      <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                  </div>
              </div>
          )
      }
      return customCard;
  }

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer2"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row position-relative">
                            <div className="totalEarning text-center mb-2 position-absolute">
                                <h2 className="text-white font-weight-bold mb-0 text-right">₹5000</h2>
                                <div className="text-white font10">TOTAL EARNING</div>
                            </div>

                             {/* Nav tabs */}

                            <ul class="nav nav-tabs border-0" role="tablist">
                                <li class="nav-item">
                                <a class="nav-link whiteColor active" data-toggle="tab" href="#paymentdone">PAYMENT DONE</a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link whiteColor" data-toggle="tab" href="#paymentpending">PAYMENT PENDING</a>
                                </li>
                            </ul>

                           {/* Tab panes */}

                            <div class="tab-content w-100 mt-3">
                                <div id="paymentdone" class="container tab-pane active">
                                    <div className="row">
                                    {this.loop()}
                                    </div>
                                </div>
                                <div id="paymentpending" class="container tab-pane fade">
                                    <div className="row">
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                        <div className="col-lg-2 col-md-3 p-2">
                                            <div className="customCard shadow p-3">
                                                <div className="w-100 pull-left">
                                                    <div className="bookedCeleb2 radius-full pull-right">
                                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                                    </div>
                                                </div>

                                                <b className="w-100 pull-left text-right mt-2" style={{ height: 50 }}>Nicole Coelho</b>

                                                <div className="rupees font14 font-weight-bold pull-right redf">-₹500</div>
                                                <div className="w-100 font8 grey-color pull-right text-right">20 Jul 2019</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        {/* <div className="col-lg-10 bg-white rounded shadow-lg pb-4 pt-2 pl-4 pr-4">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">PAYMENT DONE</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">PAYMENT PENDING</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right prif-color">+500</div>
                                </div>

                                
                            </div>
                            <div class="tab-pane fade " id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                <div className="w-100 pt-3 pb-3 border-bottom float-left">
                                    <div className="bookedCeleb1 radius-full float-left">
                                        <img src={require('../img/celebrity1.png')} alt='test' />
                                    </div>
                                    <div className="idmain1 float-left">
                                        <div className="w-100 d-flex align-items-center justify-content-between font9"><b className="mr-2">Rakesh Jain</b></div>
                                        <div className="w-100 font8 float-left grey-color">20 Jul 2019</div>
                                    </div>
                                    <div className="rupees font14 font-weight-bold float-right redf">-500</div>
                                </div>
                                
                                
                                </div>
                            </div>
                        </div> */}
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Mytrasaction;