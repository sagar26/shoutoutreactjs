import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Contactus extends Component {
  render() {
    return (
      <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4">
                    <h5 className="prif-color font-weight-bold mb-3">Contact us</h5>

                    <div className="col-lg-4 shadow1 mb-3 p-3">
                      <div className="grey-color border-bottom pb-2">Name</div>
                      <div className="font12 font-weight-bold pt-2">Melina Fernandes</div>
                    </div>
                    <div className="col-lg-4 shadow1 mb-3 p-3">
                      <div className=" border-bottom pb-2">Email Id</div>
                      <a href="mailto:melinaF@gmail.com" style={{color:"#212529"}} className="font12 font-weight-bold pt-2 d-flex align-content-center justify-content-between">melinaF@gmail.com<i class="fa fa-at font14 mt-1 green"></i></a>
                    </div>

                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Contactus;