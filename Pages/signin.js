import React, { Component,AsyncStorage} from 'react';
import '../custom/login.css';
import ReactDOM from "react-dom";
// import {StoreItem} from '../component/storeitem'
import constant from '../component/constant';
import Loader from '../utils/loader'

import {
    Link
  } from 'react-router-dom'
export default class Signin extends Component {
      constructor(props){
          super(props);
          this.routeChange=this.routeChange.bind(this);
          this.handsubmit=this.handsubmit.bind(this);
          this.handleemail=this.handleemail.bind(this);
          this.handlepassword=this.handlepassword.bind(this);
          this.state={
            check:false,check1:false,showPassword: true,
            // isgo:this.props.navigation.getParam('isgo'),
            // isnumber:this.props.navigation.getParam('isnumber'),
            email:"",
            password:"",
            loading:false,
            isHidden:false,
            showPassword: true,
             }
      this.load()
     };

     load=async()=>{
        // localStorage.clear();
        const context = this;
        const name = localStorage.getItem('name', '')
        const email = localStorage.getItem('email', '')
        const token = localStorage.getItem('token', '')
        const text = 'Session timeout ! \n  Please login again.';
        console.log(name+','+email+','+token);
            // if (name !== null || email !== null || token !== null) {
            //     this.props.history.push('/home');
            // }
            // else{
            //   let newText = text.split('\n').map(item => {
            //     return item
            // });
            //   alert(newText);
            // }
    
            
      }
      emailValidator = (email) => {
          var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return re.test(String(email).toLowerCase());
      }
        
      routeChange(){
          let path="/signup";
          this.props.history.push(path);
      }

  handsubmit = async (e) => {
    if (this.state.password.length > 1 && this.state.email.length > 1) {
      this.signinUser();
    }else{
      alert('please fill detail')
    }
    console.log(this.state.email)
    console.log(this.state.password)
    // if(this.state.email === "celebrity" && this.state.password === '123456'){
    //     // alert("celebrity");
    //     // StoreItem('userData','2')
    //     localStorage.setItem('clicks',"2");
    //       this.props.history.push('/home');

    // }else if(this.state.email === 'customer' && this.state.password === '123456'){
    //     // alert("customer");
    //     // StoreItem('userData','1')
    //     localStorage.setItem('clicks', "1");
    //       this.props.history.push('/home');
    // }else{
    //     alert('please fill details')
    // }
    e.preventDefault();
  }
    
      signinUser=()=>{
        this.setState({loading: true});
        var url=constant.SER_URL+'/login';
        console.log('hit',url)
        let body ={email:this.state.email,
            password: this.state.password,
            device_id:'231',
            firebase_token:'231213',
            platform:'000',
            login_type:'1'
          }
            console.log(body)
        fetch(url,{
            method:'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((response) =>{
              var resmsg = response['code'];
              console.log(resmsg)
              if (resmsg == "200"){
                this.setState({loading: true});
                setTimeout(() => { this.setState({loading: false}); }, 1000);
                console.log('Logged in!');
                this.setState({
                  userdata:response['data']
                })
                localStorage.setItem('userData',JSON.stringify(this.state.userdata));
                localStorage.setItem('name',JSON.stringify(this.state.userdata.name));
                localStorage.setItem('email',JSON.stringify(this.state.userdata.email));
                localStorage.setItem('token',JSON.stringify(this.state.userdata.token));
                this.props.history.push('/home');
              }
              else if (resmsg == "400") {
                this.setState({loading: true});
                setTimeout(() => { this.setState({loading: false}); });
                
                if(response['data'].length != ''){
                  console.log(JSON.stringify(response['message']));
                //   this.props.navigation.navigate('VerifyScreen',{isnumber:this.state.email,otp:response['data']['otp'],isfrom:'login'})
                }else{
                  console.log(JSON.stringify(response['message']));
                }
              }
              else if (resmsg == "422") {
                this.setState({loading: true}); 
                setTimeout(() => { this.setState({loading: false}); });
                
                console.log(JSON.stringify(response['data']));
              }
            }).catch(error => {
            this.setState({
                loading: false
            });
        });
    }
    

        handleemail=(e)=>{
            this.setState({ email: e.target.value })
        }
        handlepassword=(e)=>{
            this.setState({ password: e.target.value })
        }
  render() {
    return (
    <div className="container-fluid h-100">
        <div className="row h-100">
            <div className="login-left pribg-color h-100 login-left-bg">
                
            </div>
            <div className="login-right h-100 ">
                    <div className="celebrity-block w-100">
                        <div className="pl-4 pr-4 mb-2">
                            <h5 className="prif-color font-weight-bold w-100 font16">Klippy</h5>
                            <h6 className="prif-color font-weight-bold w-100 pb-4 font12">Your very own celebrity message!</h6>
                            <form onSubmit={this.handsubmit} className='w-100'>
                                <div className="form-group">
                                    <input type="text" className="form-control" id="email" placeholder="Enter email" name="email" onChange={this.handleemail}></input>
                                </div>
                                <div className="form-group mb-1">
                                    <input  type="password" className="form-control" id="pwd" placeholder="Enter password" name="pswd" secureTextEntry={this.state.showPassword} onChange={this.handlepassword}></input>
                                </div>
                                <div className="form-group text-right">
                                    <Link to='/forgot' className="btn btn-light font10 font-weight-bold grey-color pt-0 pb-0 font8">
                                        FORGOT PASSWORD
                                    </Link>
                                </div>
                                <div className="form-group d-flex justify-content-center pt-4">
                                    <button type="submit" className="btn btn-info btn-lg pl-4 pr-4 d-flex align-items-center font10 font-weight-bold radius-full pribg-color">
                                        <span className="pull-left">SIGN IN</span>
                                        <i className="fa fa-long-arrow-right pull-right"></i>
                                    </button>
                                </div>
                                <div className="form-group d-flex justify-content-center">
                                    <button onClick={this.routeChange} type="button" className="btn pl-4 pr-4 d-flex align-items-center font-weight-bold grey-color font8">
                                        <u className="pull-left">I am new, register me!</u>
                                    </button>
                                </div>

                            </form>
                        </div>

                    </div>
            </div>
        </div>
        <Loader loading={this.state.loading} />
    </div>
    );
  }
}

