import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
import { Link } from 'react-router-dom'
  

class Faq extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4">
                        <h5 className="prif-color font-weight-bold mb-3">FAQ's</h5>
                        <div id="accordion" class="accordion">
                            <div class=" mb-0">
                                <div className="shadow1 mb-3 ">
                                <div class="card-header collapsed bg-white" data-toggle="collapse" href="#collapseOne">
                                    <Link to="/" class="card-title font-weight-bold text-dark">Lorem Ipsum is simply dummy ?</Link>
                                </div>
                                <div id="collapseOne" class="card-body collapse" data-parent="#accordion" >
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                        aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                                </div>
                                <div className="shadow1 mb-3">
                                <div class="card-header collapsed bg-white" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                    <Link to="/" class="card-title font-weight-bold text-dark">Lorem Ipsum is simply dummy ?</Link>
                                </div>
                                <div id="collapseTwo" class="card-body collapse" data-parent="#accordion" >
                                    <p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                        aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                        craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
                                </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Faq;