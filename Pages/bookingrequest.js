import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';
// import ReactPlayer from 'react-player';

class Bookingrequest extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container">
                        <div className="row justify-content-center">
                        <div className="col-lg-6 bg-white rounded shadow-lg p-4">
                            <form className="w-100">
                                <h5 className="w-100 mb-4">Request for</h5>
                               
                                <div class="form-group w-100">
                                    <select class="form-control bg-warning text-white border-0" id="sel1">
                                        <option>Other</option>
                                        <option>My Self</option>
                                    </select>
                                </div>
                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="usr">My name is</label>
                                    <input type="text" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="usr" />
                                </div>
                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="dedicate">I dedicate this shoutout to</label>
                                    <input type="email" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="dedicate" />
                                </div> 
                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="desc">And my description is</label>
                                    <textarea type="email" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="desc" />
                                </div>

                                <h5 className="w-100 mb-4">Delivery Information</h5>

                                <div class="form-group w-100">
                                    <label className="font9 mb-0" for="email">My email </label>
                                    <input type="email" class="form-control pl-0 border-top-0 border-left-0 border-right-0 rounded-0" id="email" />
                                </div> 
                                
                                <div className="w-100 text-center">
                                <button className="btn font-weight-bold pribg-color radius-full text-white">
                          REQUEST SACHIN TENDULKER<b className="ml-3 font14">₹100</b></button>
                                </div>
                            </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Bookingrequest;