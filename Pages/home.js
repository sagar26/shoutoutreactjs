import React, {Component} from 'react';
import {
    Link
} from 'react-router-dom'
import Header from '../component/header';
import Footer from '../component/footer';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import {retrieveItem} from '../component/retriveitem';
import constant from '../component/constant';
import Newsktn from '../component/skton';
import Loader from '../utils/loader'
import Toast from '../utils/toast'
import {messaging} from "../utils/init-fcm";


class Home extends Component {

    constructor(props) {
        super(props);
        this.state = {
            scro: false,
            loading: false,
            loadsk: false,
            modalVisible: false,
            dataSource: [],
            isvisible: false,
            countlisty: 1,
            text: '',
            textvalue: '',
            inputValue: '',
            toast: false
        };
        // this.routeChange = this.routeChange.bind(this);
        this.getusertype();
        this.newuser();
        this.notification();
    }

    getusertype = async () => {
        this.setState({toast: false});
        // let userdata =localStorage.getItem('userData');
        // console.log("userdata:-"+userdata)
        //   retrieveItem
        await retrieveItem('userData').then((item) => {

            setTimeout(() => {
                this.setState({toast: true});
            }, 1000);

            console.log('retrieveItem:- ' + item)
            this.setState({
                currentdata: item,
                otp: item.otp_verified,
                name: item.name,
                email: item.email,
                updatedat: item.updated_at,
                createdat: item.created_at,
                id: item.id,
                usertype: item.user_type,
                price: item.price,
                token: item.token
            });

        }).catch((error) => {
            console.log('Promise is rejected with erro', error)
        })
        setTimeout(() => {
            this.setState({toast: false});
        }, 3000);
        //   //   retrieveItem
        //   // this.refs.toast.show('Welcome, '+this.state.name, Toasty);
        // alert('Welcome, '+this.state.name)

    }

    componentDidMount() {
        this.checkPermission()
    }

//1
    async checkPermission() {
        try {
            await messaging.requestPermission();
            // User has authorised
            this.getToken();
            console.log('getToken2')
        } catch (error) {
            console.log('permission rejected')
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

//3
    async getToken() {
        let fcmToken = await messaging.getToken();
        if (fcmToken) {
            // user has a device token
            console.log('fcmtoken-- ' + fcmToken)

        } else {
            // user doesn't have a device token yet
            console.log('no fcmtoken')
        }
    }

// async requestpremission(){
//   messaging.requestPermission()
//   .then(async function() {
//     const token = await messaging.getToken();
//     console.log('token '+token)
//   })
//   .catch(function(err) {
//     console.log("Unable to get permission to notify.", err);
//   });
// navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
// messaging.onMessage((payload) => console.log('Message received. ', payload))
// }

    // getusertype = async()=>{
    //   // retrieveItem
    //   // var storedClicks;
    //   // if (localStorage.getItem('userData')) {
    //   //   storedClicks = parseInt(localStorage.getItem('clicks'));
    //   // }

    //   // this.state = {
    //   //   clicks: storedClicks,
    //   // };
    //   //   // retrieveItem
    //   //   alert('Welcome, '+this.state.clicks);

    //   }
    myCallback = (item) => {
        this.setState({item: item});
    }
    routeChange = (e) => {
        // var op={
        //   response:ev.target.dataset.value
        // }
        let val = e.currentTarget.getAttribute('data-value')
        // let path = '/booking/';
        // this.props.history.push('/booking');
        window.location.href = "/booking/" + val;
        console.log(val)
    }


    newuser = async () => {
        this.setState({loading: true});
        var url = constant.SER_URL + '/home';
        await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then((response) => response.json())
            .then((response) => {
                console.log("Response", response)
                var resmsg = response['code'];
                if (resmsg == "200") {
                    this.setState({loading: true});
                    setTimeout(() => {
                        this.setState({loading: false});
                    }, 2000);
                    this.setState({
                        newuserdata: response['data']['newUsers'],
                        trendingdata: response['data']['trending']
                    })
                    setTimeout(() => {
                        this.setState({
                            loadsk: true
                        })
                    }, 1000);
                    // alert(JSON.stringify(response['data']));

                }
                else if (resmsg == "400") {
                    this.setState({loading: true});
                    setTimeout(() => {
                        this.setState({loading: false});
                    }, 2000);
                    alert('400');
                }
                else if (resmsg == "422") {
                    this.setState({loading: true});
                    setTimeout(() => {
                        this.setState({loading: false});
                    }, 2000);
                    alert(JSON.stringify(response['data']));
                }
            }).catch(error => {
                this.setState({
                    loading: false
                });
            });
    }
    notification = async () => {
        this.setState({loading: true});
        var url = 'http://192.168.2.42/celebrity-pattern/public/api/v1/notification-list';
        await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vbG9jYWxob3N0L2NlbGVicml0eS1wYXR0ZXJuL3B1YmxpYy9hcGkvdjEvbG9naW4iLCJpYXQiOjE1NjUwNjg2NzYsImV4cCI6MTU5NjYwNDY3NiwibmJmIjoxNTY1MDY4Njc2LCJqdGkiOiJJQ1VzcWFBdmdWY1hCcmlOIiwic3ViIjo0LCJwcnYiOiI2MTM0MDBlZmIxYmZiNTBmOTZmOTBiM2VmZjNlYjFlNDlkZjRlODFkIn0.bJkjY4ClQ00_Q9vYVQulDqPR0-ZwiSlmTE_9nOjijRA'
            },
            body: {
                celebrity_id: 5
            }
        })
            .then(response => response.json())
            .then((responseJson) => {
                // console.log('comin1234' + JSON.stringify(responseJson));
                this.setState({
                    dataSource: responseJson['data']
                })
            })
            .catch(error => console.log(error)) //to catch the errors if any
    }


    loop = () => {
        let array = []
        for (let i = 1; i < 20; i++) {
            array.push(<div onClick={this.routeChange.bind()} key={i} className='item category-list-main pl-4 pr-4 '>
                    <div className="w-100 d-flex justify-content-center">
                        <div className='category-list radius-full'>
                            <img src={require('../img/celebrity1.png')} alt='test'/>
                        </div>
                    </div>
                    <h6 className='text-center mt-1 mb-0 font10 w-100 text-ellipsis'>Lisa Headen {i}</h6>
                    <p className='text-center mt-0 grey-color font8 w-100 text-ellipsis'>Actor {i}</p>
                </div>
            )
        }
        return array
    }

    notificationLoop = () => {
        let notifyArray = [];

        for (let i = 0; i < this.state.dataSource.length; i++) {
            notifyArray.push(
                <div className="notification-card">
                    <div className="notify-header">
                        <div className="notification-title">
                            <h4>{this.state.dataSource[i].title}</h4>
                        </div>
                        <div className="date-time">
                            <p>{this.state.dataSource[i].created_at}</p>
                        </div>
                    </div>
                    <p>{this.state.dataSource[i].message}</p>

                </div>
            )
        }
        return notifyArray
    }

    updateInputValue = (evt) => {
        this.setState({
            isvisible: !this.state.isvisible,
            inputValue: evt.target.value
        })
        var textv = evt.target.value;
        const newData = this.state.trendingdata.filter(item => {
            const itemData = `${item.name}`;
            const itemData1 = itemData.toLowerCase();
            const textData = textv.toLowerCase();
            return itemData1.indexOf(textData) > -1;
        })
        this.setState({loadsearData: newData, countlisty: newData.length});
        this.loadstate(newData);
        // console.log("Typing: ",textv)
        // console.log(JSON.stringify(newData))
    }
    loadstate = (item) => {
        if (this.state.text != '' || this.state.text != null || this.state.text != undefined)
            this.setState({datalo: item, focus: true})
        else
            this.setState({datalo: item, focus: true})
    }
    categories = (param) => {
        const newData = this.state.trendingdata.filter(item => {
            const newe = item.categories.map((items) => items.name)
            const itemData = `${newe}`;
            return itemData.indexOf(param) > -1;
        })
        // this.setState({ newdataload: newData,countlisty:newData.length });
        // console.log("newData"+JSON.stringify(newData))
    }


    render() {
        return (

            <div>
                {/* <Header/> */}
                <Toast toast={this.state.toast} Stext={'Welcome'} Sbody={this.state.name ? this.state.name : "Guest"}/>
                <header>
                    <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

                        {/* set booking price popup start */}
                        <div className="modal fade" id="myModal">
                            <div className="modal-dialog modal-sm">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title">Set booking price</h5>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div className="modal-body">
                                        <form>
                                            <div class="form-group mb-0">
                                                <input type="number" class="form-control border-0"
                                                       placeholder="Enter price" name="text1"/>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* set booking price popup start */}

                        {/* update mobile popup start */}
                        <div className="modal fade" id="updateMobile">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                                        <button type="button" className="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div className="modal-body">
                                        <form>
                                            <div class="form-group mb-0">
                                                <input type="number" class="form-control border-0"
                                                       placeholder="Enter mobile number" name="text1"/>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-primary" data-dismiss="modal">SEND
                                            OTP
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* update mobile popup start */}

                        <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#collapsibleNavbar">
                            <span className="navbar-toggler-icon"></span>
                        </button>
                        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
                            {
                                this.state.id ?
                                    <ul className="navbar-nav font-weight-bold">
                                        <li className="nav-item">
                                            <Link to="/signout" className="nav-link loginText">Sign out</Link>
                                        </li>
                                    </ul>
                                    :
                                    <ul className="navbar-nav font-weight-bold">
                                        <li className="nav-item">
                                            <Link to="/signup" className="nav-link loginText">Sign up</Link>
                                        </li>
                                        <li className="nav-item">
                                            <Link to="/signin" className="nav-link loginText">Sign in</Link>
                                        </li>
                                    </ul>
                            }
                            <div className="after-login">

                                <div className=" d-flex align-items-center">
                                    <div className="profile-img float-left ml-3 mr-2">
                                        <img src='https://i.imgur.com/oJKMjGR.png'/>
                                    </div>
                                    <li className="nav-item dropdown list-unstyled">
                                        <span
                                            class={this.state.usertype != null || this.state.usertype != undefined ? "nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold" : "nav-link  pl-0 pr-0 text-white font-weight-bold"}
                                            href="#" id="navbardrop"
                                            data-toggle="dropdown">{this.state.name ? this.state.name : "Guest"}</span>
                                        {
                                            this.state.usertype != null || this.state.usertype != undefined ?
                                                <div className="dropdown-menu dropdown-menu-right">
                                                    <span data-value={this.state.id ? this.state.id : '0'}
                                                          onClick={(k) => this.routeChange(k)} class="dropdown-item">View profile</span>
                                                    <Link to="/updateprofile" class="dropdown-item">Update
                                                        profile</Link>
                                                    {
                                                        this.state.usertype == '2' ?
                                                            <Link to="/mytransaction" class="dropdown-item">My
                                                                transaction</Link> : null
                                                    }

                                                    <Link to="/bookinghistory" class="dropdown-item">Booking
                                                        history</Link>
                                                </div> : null
                                        }

                                    </li>
                                </div>

                                <div className="float-left text-white-50 pl-3 pr-2">|</div>
                                <div className="dropdown">
                                    <button class="btn pl-1 pr-1 position-relative" type="button" id="notificationID"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-bell-o text-white"></i>
                                        <div
                                            className="notify-point font40 position-absolute">{this.state.dataSource ? this.state.dataSource.length : '0'}</div>
                                    </button>
                                    <div className="dropdown-menu dropdown-menu-right wid-300"
                                         aria-labelledby="notificationID">
                                        <div className="notification-container">
                                            {/*{this.notificationLoop()}*/}
                                            {
                                                this.state.dataSource ?
                                                    this.state.dataSource.slice(0,5).map((item, k) =>
                                                        <div className="notification-card" key={k}>
                                                            <div className="notify-header">
                                                                {/*<div className="notification-title">*/}

                                                                {/*<h4>{item.title}</h4>*/}

                                                                {/*</div>*/}
                                                                <div className="date-time">
                                                                    <p>{item.created_at}</p>
                                                                </div>
                                                            </div>
                                                            <p>{item.message}</p>
                                                        </div>
                                                    ) : <p>
                                                        <center>no notification</center>
                                                    </p>


                                            }
                                            {
                                                this.state.dataSource.length > 5 ?
                                                    <center>
                                                        <button
                                                            className="btn btn-default"
                                                            onClick={this.props.handleClick}>{this.props.label}<Link
                                                            to="/seemore" class="dropdown-item">SEE MORE</Link></button>
                                                    </center>
                                                    : null

                                            }
                                            {/*<div className="notification-card">*/}
                                            {/*<div className="notify-header">*/}
                                            {/*<div className="notification-title">*/}
                                            {/*<h4>Notification Heading</h4>*/}
                                            {/*</div>*/}
                                            {/*<div className="date-time">*/}
                                            {/*<p>Today</p>*/}
                                            {/*</div>*/}
                                            {/*</div>*/}
                                            {/*<p>update details</p>*/}
                                            {/*</div>*/}
                                            {/*<div className="notification-card">*/}
                                            {/*<div className="notify-header">*/}
                                            {/*<div className="notification-title">*/}
                                            {/*<h4>Notification Heading</h4>*/}
                                            {/*</div>*/}
                                            {/*<div className="date-time">*/}
                                            {/*<p>Yesterday</p>*/}
                                            {/*</div>*/}
                                            {/*</div>*/}

                                            {/*<p>update details</p>*/}
                                            {/*</div>*/}
                                            {/*<div className="notification-card">*/}
                                            {/*<div className="notify-header">*/}
                                            {/*<div className="notification-title">*/}
                                            {/*<h4>Notification Heading</h4>*/}
                                            {/*</div>*/}
                                            {/*<div className="date-time">*/}
                                            {/*<p>This Week</p>*/}
                                            {/*</div>*/}
                                            {/*</div>*/}
                                            {/*<p>update details</p></div>*/}
                                        </div>
                                    </div>

                                </div>


                                <div class="dropdown">
                                    <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-ellipsis-v text-white"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <Link to="/aboutus" class="dropdown-item">About us</Link>
                                        <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                                        <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                                        <Link to="/faq" class="dropdown-item">FAQ</Link>
                                        <Link to="/contactus" class="dropdown-item">Contact us</Link>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </nav>
                </header>
                {/* <button id="h1">Hello React js</button> */}
                <div className="container-fluid p-0 homebluecontainer">
                    <div className="pribg-color pt-4 pb-4">
                        <div className="row justify-content-center align-items-center m-0">
                            <div className="col-sm-6 col-md-6">
                                <form>
                                    <div className="form-group has-search position-relative">
                                        <div className="form-group">
                                            {
                                                this.state.isvisible ?
                                                    <span
                                                        class="spinner-border spinner-border-sm form-control-feedback top11"></span> :
                                                    <span className="fa fa-search form-control-feedback"></span>
                                            }


                                            <input id="searchInput" value={this.state.inputValue}
                                                   onChange={evt => this.updateInputValue(evt)}
                                                   className="form-control radius-full" placeholder="Search"/>
                                            {
                                                this.state.isvisible ?
                                                    <div id="searchInputBox" className="searchBox shadow-lg">
                                                        <div className="innerSearchBox">
                                                            {
                                                                this.state.loadsk ?
                                                                    this.state.countlisty > 0 || this.state.datalo.length > 0 ?
                                                                        this.state.datalo
                                                                            .sort(function (a, b) {
                                                                                if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;
                                                                                if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;
                                                                                return 0;
                                                                            })
                                                                            .map((item, k) =>
                                                                                <div
                                                                                    onClick={this.routeChange.bind(item)}
                                                                                    key={k}
                                                                                    className='category-list-main height-auto'>
                                                                                    <div className="w-100">
                                                                                        <div
                                                                                            className='category-list search-category-list radius-full imgi float-left'>
                                                                                            <img
                                                                                                data-src={item.profile_pic ? item.profile_pic : 'https://i.imgur.com/oJKMjGR.png'}
                                                                                                alt={item.name}
                                                                                                className='lazy-img'
                                                                                                src={require('../img/celebrity1.png')}/>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div
                                                                                        className="search-celebDetails float-left">
                                                                                        <div className="pl-2">
                                                                                            <h6 className='text-left mt-1 mb-0 font10 w-100'>{item.name}</h6>
                                                                                            <p className='text-left mt-0 mb-0 grey-color font8 w-100 d-flex'>{item.categories.map((items, i) =>
                                                                                                <p className='mb-0 grey-color font8 mr-1 ml-1'
                                                                                                   key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)} </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>) : <div style={{
                                                                            flex: 1,
                                                                            justifyContent: 'center',
                                                                            alignItems: 'center',
                                                                            marginTop: 35
                                                                        }}>
                                                                            <div style={{
                                                                                width: "90%",
                                                                                justifyContent: 'center',
                                                                                alignItems: 'center',
                                                                            }}>
                                                                                <p style={{
                                                                                    color: "#2B2B2B",
                                                                                    textTransform: "uppercase",
                                                                                    fontSize: 25,
                                                                                    textAlign: 'center',
                                                                                    marginBottom: 25
                                                                                }}>Don't see who you're looking for?
                                                                                    Request {this.state.inputValue}</p>
                                                                            </div>
                                                                        </div>
                                                                    : <Newsktn count={14} size={"100%"} sizeH={100}/>
                                                            }
                                                        </div>
                                                    </div>
                                                    : null
                                            }
                                            <div className="col-md-12 text-center mt-3 text-white font-weight-bold">
                                                Find your favourite celebrity here!
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="body-color pt-5 pb-5">
                        <div className="celebrity-block">
                            <div className="d-flex justify-content-between pl-4 pr-4 mb-2">
                                <h5 className="prif-color font-weight-bold">Actor</h5>
                                <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">
                                    See All
                                </button>
                            </div>
                            {this.state.newuserdata ?

                                <OwlCarousel
                                    className="owl-theme"
                                    dots={false}
                                    items={8}
                                    responsiveClass={true}
                                    responsive={
                                        {
                                            0: {
                                                items: 2
                                            },
                                            576: {
                                                items: 3
                                            },
                                            768: {
                                                items: 5
                                            },
                                            992: {
                                                items: 6
                                            },
                                            1024: {
                                                items: 6
                                            },
                                            1200: {
                                                items: 6
                                            }
                                        }
                                    }
                                >
                                    {
                                        this.state.newuserdata ?
                                            this.categories('ACTORS') : null}
                                    {this.state.newuserdata.map((item, k) =>
                                        item.categories.map((ite) =>
                                            ite.name == 'ACTORS' ?
                                                <div data-value={item.id} onClick={(k) => this.routeChange(k)} key={k}
                                                     className='item category-list-main pl-4 pr-4 '>
                                                    <div className="w-100 d-flex justify-content-center">
                                                        <div className='category-list radius-full imgi'>
                                                            <img
                                                                data-src={item.thumbnail_profile_pic ? item.thumbnail_profile_pic : 'https://i.imgur.com/oJKMjGR.png'}
                                                                alt={item.name} className='lazy-img'
                                                                src={'https://i.imgur.com/oJKMjGR.png'}/>
                                                        </div>
                                                    </div>
                                                    <h6 className='text-center mt-1 mb-0 font10 w-100 text-ellipsis'>{item.name}</h6>
                                                    <p className='text-center mt-0 mb-0 grey-color font8 w-100 d-flex justify-content-center'>{item.categories.map((items, i) =>
                                                        <p className='grey-color font8 mr-1 ml-1'
                                                           key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)} </p>
                                                </div> : null
                                        )
                                    )}
                                </OwlCarousel>
                                :
                                <div style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                    paddingLeft: 15,
                                    paddingRight: 15
                                }}>
                                    <Newsktn count={6} size={169} sizeH={198}/>
                                </div>
                            }
                        </div>
                        <div className="celebrity-block">
                            <div className="d-flex justify-content-between pl-4 pr-4 mb-2">
                                <h5 className="prif-color font-weight-bold">Sports</h5>
                                <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">
                                    See All
                                </button>
                            </div>
                            {
                                this.state.newuserdata ?
                                    this.categories('SPORTS') : null}
                            {this.state.newuserdata ?

                                <OwlCarousel
                                    className="owl-theme"
                                    dots={false}
                                    items={8}
                                    responsiveClass={true}
                                    responsive={
                                        {
                                            0: {
                                                items: 2
                                            },
                                            576: {
                                                items: 3
                                            },
                                            768: {
                                                items: 5
                                            },
                                            992: {
                                                items: 6
                                            },
                                            1024: {
                                                items: 6
                                            },
                                            1200: {
                                                items: 6
                                            }
                                        }
                                    }
                                >
                                    {this.state.newuserdata.map((item, k) =>
                                        item.categories.map((ite, i) =>
                                            i < 10 ?
                                                ite.name == 'SPORTS' ?
                                                    <div data-value={item.id} onClick={(k) => this.routeChange(k)}
                                                         className='item category-list-main pl-4 pr-4 '>
                                                        <div className="w-100 d-flex justify-content-center">
                                                            <div className='category-list radius-full imgi'>
                                                                <img
                                                                    data-src={item.thumbnail_profile_pic ? item.thumbnail_profile_pic : 'https://i.imgur.com/oJKMjGR.png'}
                                                                    alt={item.name} className='lazy-img'
                                                                    src={'https://i.imgur.com/oJKMjGR.png'}/>
                                                            </div>
                                                        </div>
                                                        <h6 className='text-center mt-1 mb-0 font10 w-100 text-ellipsis'>{item.name}</h6>
                                                        <p className='text-center mt-0 mb-0 grey-color font8 w-100 d-flex justify-content-center'>{item.categories.map((items, i) =>
                                                            <p className='grey-color font8 mr-1 ml-1'
                                                               key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)} </p>
                                                    </div> : null : null
                                        )
                                    )}
                                </OwlCarousel>
                                :
                                <div style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                    paddingLeft: 15,
                                    paddingRight: 15
                                }}>
                                    <Newsktn count={6} size={169} sizeH={198}/>
                                </div>
                            }
                        </div>

                        <div className="celebrity-block">
                            <div className="d-flex justify-content-between pl-4 pr-4 mb-2">
                                <h5 className="prif-color font-weight-bold">INSTAGRAM & YOUTUBE STARS</h5>
                                <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">
                                    See All
                                </button>
                            </div>
                            {
                                this.state.newuserdata ?
                                    this.categories('INSTAGRAM & YOUTUBE STARS') : null}
                            {this.state.newuserdata ?

                                <OwlCarousel
                                    className="owl-theme"
                                    dots={false}
                                    items={8}
                                    responsiveClass={true}
                                    responsive={
                                        {
                                            0: {
                                                items: 2
                                            },
                                            576: {
                                                items: 3
                                            },
                                            768: {
                                                items: 5
                                            },
                                            992: {
                                                items: 6
                                            },
                                            1024: {
                                                items: 6
                                            },
                                            1200: {
                                                items: 6
                                            }
                                        }
                                    }
                                >
                                    {this.state.newuserdata.map((item, k) =>
                                        item.categories.map((ite, i) =>
                                            i < 10 ?
                                                ite.name == 'INSTAGRAM & YOUTUBE STARS' ?
                                                    <div data-value={item.id} onClick={(k) => this.routeChange(k)}
                                                         className='item category-list-main pl-4 pr-4 '>
                                                        <div className="w-100 d-flex justify-content-center">
                                                            <div className='category-list radius-full imgi'>
                                                                <img
                                                                    data-src={item.thumbnail_profile_pic ? item.thumbnail_profile_pic : 'https://i.imgur.com/oJKMjGR.png'}
                                                                    alt={item.name} className='lazy-img'
                                                                    src={'https://i.imgur.com/oJKMjGR.png'}/>
                                                            </div>
                                                        </div>
                                                        <h6 className='text-center mt-1 mb-0 font10 w-100 text-ellipsis'>{item.name}</h6>
                                                        <p className='text-center mt-0 mb-0 grey-color font8 w-100 d-flex justify-content-center'>{item.categories.map((items, i) =>
                                                            <p className='grey-color font8 mr-1 ml-1'
                                                               key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)} </p>
                                                    </div> : null : null
                                        )
                                    )}
                                </OwlCarousel>
                                :
                                <div style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                    paddingLeft: 15,
                                    paddingRight: 15
                                }}>
                                    <Newsktn count={6} size={169} sizeH={198}/>
                                </div>
                            }
                        </div>
                        <div className="celebrity-block">
                            <div className="d-flex justify-content-between pl-4 pr-4 mb-2">
                                <h5 className="prif-color font-weight-bold">MUSICIANS & SINGERS</h5>
                                <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">
                                    See All
                                </button>
                            </div>
                            {
                                this.state.newuserdata ?
                                    this.categories('MUSICIANS & SINGERS') : null}
                            {this.state.newuserdata ?

                                <OwlCarousel
                                    className="owl-theme"
                                    dots={false}
                                    items={8}
                                    responsiveClass={true}
                                    responsive={
                                        {
                                            0: {
                                                items: 2
                                            },
                                            576: {
                                                items: 3
                                            },
                                            768: {
                                                items: 5
                                            },
                                            992: {
                                                items: 6
                                            },
                                            1024: {
                                                items: 6
                                            },
                                            1200: {
                                                items: 6
                                            }
                                        }
                                    }
                                >
                                    {this.state.newuserdata.map((item, k) =>
                                        item.categories.map((ite, i) =>
                                            i < 10 ?
                                                ite.name == 'MUSICIANS & SINGERS' ?
                                                    <div data-value={item.id} onClick={(k) => this.routeChange(k)}
                                                         className='item category-list-main pl-4 pr-4 '>
                                                        <div className="w-100 d-flex justify-content-center">
                                                            <div className='category-list radius-full imgi'>
                                                                <img
                                                                    data-src={item.thumbnail_profile_pic ? item.thumbnail_profile_pic : 'https://i.imgur.com/oJKMjGR.png'}
                                                                    alt={item.name} className='lazy-img'
                                                                    src={'https://i.imgur.com/oJKMjGR.png'}/>
                                                            </div>
                                                        </div>
                                                        <h6 className='text-center mt-1 mb-0 font10 w-100 text-ellipsis'>{item.name}</h6>
                                                        <p className='text-center mt-0 mb-0 grey-color font8 w-100 d-flex justify-content-center'>{item.categories.map((items, i) =>
                                                            <p className='grey-color font8 mr-1 ml-1'
                                                               key={i}>{items.name}{items[items.length - 1] == items.name ? null : ' '}</p>)} </p>
                                                    </div> : null : null
                                        )
                                    )}
                                </OwlCarousel>
                                :
                                <div style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    flexWrap: "wrap",
                                    paddingLeft: 15,
                                    paddingRight: 15
                                }}>
                                    <Newsktn count={6} size={169} sizeH={198}/>
                                </div>
                            }
                        </div>

                        {/* <div style={{display:"flex",flexDirection:"row",flexWrap:"wrap",paddingLeft:15,paddingRight:15}}>
            <Newsktn count={6} size={169} sizeH={198}/>
          </div> */}
                        {/* <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Comedians</h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">DJ'S & VJ'S</h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Instagram & Youtube Stars </h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Models</h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Musicians & Singers </h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Reality Stars</h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div>
      <div className="celebrity-block">
        <div className="d-flex justify-content-between pl-4 pr-4 mt-4 mb-2">
          <h5 className="prif-color font-weight-bold">Sports</h5>
          <button type="button" className="btn btn-light font10 bg-transparent pt-0 pb-0 mb-1">See All</button>
        </div>
        <OwlCarousel
            className="owl-theme"
            loop
            dots={false}
            items={8}
            responsiveClass={true}
            responsive={
              {
                0: {
                  items: 2
                },
                576: {
                  items: 3
                },
                768: {
                  items: 5
                },
                992: {
                  items: 6
                },
                1024: {
                  items: 6
                },
                1200: {
                  items: 6
                }
              }
            }
        >
            {this.loop()}
        </OwlCarousel>
      </div> */}
                    </div>
                </div>
                <Footer/>
                {/* <Loader loading={this.state.loading} /> */}
            </div>
        );
    }
}

export default Home;