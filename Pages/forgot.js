import React, {Component} from 'react';
import '../custom/login.css';
import $ from 'jquery';

export default class Forgot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalopen: false
        };
    }
    sendtop=()=>{
        $('#openmodalotp').addClass('show')
        $('#openmodalotp').css('cssText','display:block')
        $('#backdrop').addClass('show')
        $('#backdrop').css('cssText','display:block')
    }
    sendtop1=()=>{
        $('#openmodalotp').removeClass('show')
        $('#openmodalotp').css('cssText','display:none')
        $('#backdrop').removeClass('show')
        $('#backdrop').css('cssText','display:none')
    }
    render() {
        return (
            <div className="container-fluid h-100">
                <div className="row h-100">
                    <div className="login-left pribg-color h-100 login-left-bg">

                    </div>
                    <div className="login-right h-100 ">
                        <div className="celebrity-block w-100">

                            {/* forgot start */}
                            <div className="pl-4 pr-4 mb-2">
                                <h5 className="prif-color font-weight-bold w-100 pb-4 font16">Forgot Password!</h5>
                                <form action="/action_page.php" className='w-100'>
                                    <p className="text-center grey-color">
                                        Please enter your mobile number to recieve OTP!
                                    </p>
                                    <div className="form-group">
                                        <input type="number" className="form-control" id="email"
                                               placeholder="Enter mobile number" name="email"></input>
                                    </div>
                                    <div className="form-group d-flex justify-content-center pt-4">
                                        <button type="button"
                                                className="btn btn-info btn-lg pl-4 pr-4 d-flex align-items-center font10 font-weight-bold radius-full pribg-color" onClick={this.sendtop()}>
                                            <span className="pull-left">SEND OTP</span>
                                            <i className="fa fa-long-arrow-right pull-right"></i>
                                        </button>
                                    </div>

                                </form>
                            </div>

                            <div className="modal fade" id="openmodalotp">
                                <div className="modal-dialog">
                                    <div className="modal-content">
                                        <div className="modal-header">
                                            <h5 className="modal-title">Verify your number!</h5>
                                            <button type="button" className="close"
                                                    data-dismiss="modal"  onClick={this.sendtop1}>&times;</button>
                                        </div>
                                        <div className="modal-body">
                                            <form>
                                                <p className="text-center grey-color">
                                                    Please enter the verification code <br/>
                                                    we just send you on your registered mobile number!
                                                </p>
                                            </form>
                                        </div>
                                        <div className="modal-footer">
                                            <button type="button" className="btn btn-primary"
                                                    data-dismiss="modal"  onClick={this.sendtop1}>VERIFY
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-backdrop" id='backdrop' style={{display:"none"}}></div>
                            {/* forgot end */}

                            {/* Verify your number start */}
                            {/* <div className="pl-4 pr-4 mb-2">
                            <h5 className="prif-color font-weight-bold w-100 pb-4 font16">Verify your number!</h5>
                            <form className='w-100'>
                                <p className="text-center grey-color">
                                Please enter the verification code <br/>
                                we just send you on your registered mobile number!
                                </p>

                                <div className="form-group">
                                    <Link to="/" className="d-flex justify-content-center align-items-center"><b className="secf-color font14 mr-2">9819568472</b><i class="fa fa-pencil font12"></i></Link>
                                </div>

                                <div className="text-center">
                                    <input className="otp-box font20 text-center mr-3" type="email" maxLength="1"/>
                                    <input className="otp-box font20 text-center mr-3" type="email" maxLength="1"/>
                                    <input className="otp-box font20 text-center mr-3" type="email" maxLength="1"/>
                                    <input className="otp-box font20 text-center" type="email" maxLength="1"/>
                                </div>

                                <div className="text-center mt-3">1:25 min | <button className="btn bg-transparent font-weight-bold pl-0">RESEND</button></div>

                                <div className="form-group d-flex justify-content-center pt-4">
                                    <button type="button" className="btn btn-info btn-lg pl-4 pr-4 d-flex align-items-center font10 font-weight-bold radius-full pribg-color">
                                        <span className="pull-left">VERIFY</span>
                                        <i className="fa fa-long-arrow-right pull-right"></i>
                                    </button>
                                </div>

                            </form>
                        </div> */}
                            {/* Verify your number start */}

                            {/* forgot start */}
                            {/* <div className="pl-4 pr-4 mb-2">
                            <h5 className="prif-color font-weight-bold w-100 pb-4 font16">Reset Password!</h5>
                            <form action="/action_page.php" className='w-100'>

                                <div className="form-group">
                                    <input type="email" className="form-control" id="email" placeholder="Enter new password" name="email"></input>
                                </div>
                                <div className="form-group">
                                    <input type="email" className="form-control" id="email" placeholder="Re-enter new password" name="email"></input>
                                </div>
                                <div className="form-group d-flex justify-content-center pt-4">
                                    <button type="button" className="btn btn-info btn-lg pl-4 pr-4 d-flex align-items-center font10 font-weight-bold radius-full pribg-color">
                                        <span className="pull-left">DONE</span>
                                        <i className="fa fa-long-arrow-right pull-right"></i>
                                    </button>
                                </div>

                            </form>
                        </div> */}
                            {/* forgot end */}


                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

