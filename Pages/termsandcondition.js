import React, { Component } from 'react';
import Header from '../component/header';
import Footer from '../component/footer';

class Termsandcondition extends Component {
  constructor(props){
    super();
    this.state={
        muted: true,
        paused:false,
        clicks: 0,
        show:true,
      }
  }
  

  render() {
    return (
        <div>
            <Header />
            <div className="container-fluid pribg-color blueContainer"></div>
            <div className="container pb-5 border-bottom">
                <div className="row">
                    <div className="container bg-white rounded shadow-lg pb-4 pt-4 pl-4 pr-4">
                    <h5 className="prif-color font-weight-bold">Terms & condition</h5>
                    <p>
                    orem ipsum dolor sit amet, consectetur adipiscing elit. Nulla posuere maximus magna, nec ullamcorper massa mattis a. Donec non aliquam sem, elementum pellentesque turpis. Suspendisse potenti. Suspendisse iaculis arcu in neque tincidunt sagittis sed eget tortor. Ut tortor libero, volutpat et sapien a, facilisis porta massa. Fusce ac varius lacus. Praesent convallis arcu vulputate, facilisis ipsum id, imperdiet sapien. Curabitur ac justo pharetra, facilisis nunc sed, maximus risus. Nam et est ornare, scelerisque nunc non, euismod velit. Maecenas tempor suscipit sem et euismod.
                    </p>
                    <p>
                    Morbi feugiat fermentum ante. Aenean in cursus urna. Nam posuere ante purus, volutpat gravida neque bibendum a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tincidunt risus id diam luctus interdum. Sed vitae tempus purus. Praesent condimentum tincidunt nisi, eget tincidunt metus gravida sed. Nam condimentum mi malesuada risus tempor facilisis. Sed hendrerit gravida cursus. Integer porta lacus a elementum mollis. Sed ut dapibus odio. Maecenas sit amet ipsum id lorem congue dignissim in vel elit.
                    </p>
                    <p>
                    Aliquam non iaculis lorem. Vivamus bibendum lorem nisl, quis aliquet mauris dapibus eu. Integer eleifend sit amet urna a euismod. Suspendisse dictum urna nec dolor tristique condimentum. Praesent eu eros neque. Vestibulum dictum risus eget egestas vulputate. Morbi dui ligula, consequat sit amet lobortis a, tempus et eros. Sed felis nunc, sollicitudin quis leo sed, varius aliquet erat. Cras ultricies, risus vel scelerisque mollis, sem elit auctor diam, sed accumsan metus augue et erat.
                    </p>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
  }
}

export default Termsandcondition;