import React, { Component } from 'react';
import {
  Link
}from 'react-router-dom'
// importing navigation 
// import Nav from './nav'
class Footer extends Component {
  render() {
    return (
      <footer>
      <div className="container">
        <div className="row d-flex justify-content-center">
          <div className="col-md-4">
            <form>
              <div className="input-group">
                <input type="text" className="form-control" placeholder="abc@email.com"/>
                <div className="input-group-append">
                  <span className="input-group-text pribg-color text-white font-weight-bolder">SEND</span>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="row d-flex justify-content-center">
          <div className="d-flex mt-2">
              <ul className="navbar-nav flex-row flex-wrap pl-3 pr-3">
                <li className="nav-item mr-3">
                  <Link to="/aboutus" className="nav-link footerText">About us</Link>
                </li>
                <li className="nav-item mr-3">
                  <Link to="/privacypolicy" className="nav-link footerText">Privacy</Link>
                </li>
                <li className="nav-item mr-3">
                  <Link to="/termsandcondition" className="nav-link footerText">Terms</Link>
                </li>
                <li className="nav-item mr-3">
                  <Link to="/" className="nav-link footerText">Career</Link>
                </li>
                <li className="nav-item mr-3">
                  <Link to="/faq" className="nav-link footerText">FAQ</Link>
                </li>
                <li className="nav-item">
                  <Link to="/contactus" className="nav-link footerText">Contact us</Link>
                </li>
              </ul>
            </div> 
        </div>

        <div className="row d-flex justify-content-center">
            <div className="footer-copyright text-center font8 grey-color">© <span id="currentDate"></span>, Klippy, All Rights Reserved</div>
         </div>

      </div>
      
    </footer>
    );
  }
}

export default Footer;