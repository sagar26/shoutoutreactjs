import React, { AsyncStorage } from 'react';

// async function retrieveItem(key) {
//     await JSON.stringify(localStorage.getItem(key))
//     return 
// }
async function retrieveItem(key) {
    try {
        const retrievedItem =  await localStorage.getItem(key);
          const item = JSON.parse(retrievedItem);
        return item;
      } catch (error) {
        console.log(error.message);
      }
      return
 }
export {
    retrieveItem,
};