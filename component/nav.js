import React, { Component } from 'react';
import {
  Link
} from 'react-router-dom'

class Nav extends Component {
  constructor(props){
    super();
    this.getusertype()
  }
  getusertype= async()=>{
    // retrieveItem
    var storedClicks;
    if (localStorage.getItem('clicks')) {
      storedClicks = parseInt(localStorage.getItem('clicks'));
    }

    this.state = {
      clicks: storedClicks,
    };
      // retrieveItem
      // alert('Welcome, '+this.state.clicks);
      
    }


  render() {
    return (

      <nav className="navbar navbar-expand-sm pribg-color navbar-dark">

{/* set booking price popup start */}
        <div className="modal fade" id="myModal">
          <div className="modal-dialog modal-sm">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Set booking price</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter price" name="text1" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SAVE</button>
              </div>
            </div>
          </div>
        </div>
{/* set booking price popup start */}

{/* update mobile popup start */}
        <div className="modal fade" id="updateMobile">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Please enter your mobile number to recieve OTP!</h5>
                <button type="button" className="close" data-dismiss="modal">&times;</button>
              </div>
              <div className="modal-body">
                <form>
                  <div class="form-group mb-0">
                    <input type="number" class="form-control border-0" placeholder="Enter mobile number" name="text1" />
                  </div>
                </form>
                </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-primary" data-dismiss="modal">SEND OTP</button>
              </div>
            </div>
          </div>
        </div>
{/* update mobile popup start */}

        <Link to="/" className="font-weight-bold font18 text-white">KLIPPY</Link>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse flex-row-reverse" id="collapsibleNavbar">
          <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
              <Link to="/signup" className="nav-link loginText">Sign up</Link>
            </li>
            <li className="nav-item">
            <Link to="/signin" className="nav-link loginText">Sign in</Link>
            </li>    
          </ul>
          {/* <ul className="navbar-nav font-weight-bold">
            <li className="nav-item">
            <Link to="/signout" className="nav-link loginText">Sign out</Link>
            </li>    
          </ul> */}
          <div className="after-login">
          
          {/* <Link to="/follow">
            <span class="badge badge-primary mr-3">FOLLOWING 4K</span>
            {
              this.state.clicks == "1"?
              null:<span class="badge badge-primary ">FOLLOWERS 5K</span>
            }
          </Link> */}

          <div className=" d-flex align-items-center">
          <div className="profile-img float-left ml-3 mr-2"><img src={require('../img/celebrity1.png')} alt="" /></div>
                <li className="nav-item dropdown list-unstyled">
                  <span class="nav-link dropdown-toggle pl-0 pr-0 text-white font-weight-bold" href="#" id="navbardrop" data-toggle="dropdown">
                  Ishani Coelho</span>
                  <div className="dropdown-menu dropdown-menu-right">
                    <Link to="/updateprofile" class="dropdown-item">Update profile</Link>
                    {/* <span data-toggle="modal" data-target="#updateMobile" class="dropdown-item">Update mobile</span> */}
                    {/* <Link to="/enterbankdetails" class="dropdown-item">Enter bank details</Link> */}
                    {/* <span data-toggle="modal" data-target="#myModal" class="dropdown-item">
                      <p className="mb-1">Set booking price</p>
                      <b className="w-100 font14 orange-font">₹5000</b>
                    </span> */}
                    <Link to="/mytrasaction" class="dropdown-item">My transaction</Link>
                    <Link to="/bookinghistory" class="dropdown-item">Booking history</Link>
                    {/* <Link to="/booking" class="dropdown-item">Booking</Link> */}
                  </div>
                </li>
            </div>

            <div className="float-left text-white-50 pl-3 pr-2">|</div>

            <button class="btn pl-1 pr-1 position-relative"><i class="fa fa-bell-o text-white"></i>
              <div className="notify-point font40 position-absolute">.</div>
            </button>

            <div class="dropdown">
              <button class="btn" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-ellipsis-v text-white"></i>
              </button>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                <Link to="/aboutus" class="dropdown-item">About us</Link>
                <Link to="/termsandcondition" class="dropdown-item">Terms & Condition</Link>
                <Link to="/privacypolicy" class="dropdown-item">Privacy Policy</Link>
                <Link to="/faq" class="dropdown-item">FAQ</Link>
                <Link to="/contactus" class="dropdown-item">Contact us</Link>
              </div>
            </div>
          </div>
         
        </div>
      </nav>
     );
  }
}

export default Nav;
